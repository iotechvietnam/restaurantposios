//
//  GoogleMaterialIconFont.swift
//  Pods
//
//  Created by Yusuke Kita on 9/23/15.
//
//

import UIKit

public extension String {
    public static func materialIcon(font: MaterialIconFont) -> String {
        return IconFont.codes[font.rawValue]
    }
}

public extension NSString {
    public static func materialIcon(font: MaterialIconFont) -> NSString {
        return NSString(string: String.materialIcon(font: font))
    }
}

public extension UIFont {
    
    public static func materialIconOfSize(size: CGFloat) -> UIFont {
        //        var onceToken: dispatch_once_t = 0
        
        let filename = "MaterialIcons-Regular"
        let fontname = "Material Icons"
        
        
        //        if UIFont.fontNamesForFamilyName(fontname).isEmpty {
        //            dispatch_once(&onceToken, { () -> Void in
        //                FontLoader.loadFont(filename)
        //            })
        //        }
        
        if UIFont.fontNames(forFamilyName: fontname).isEmpty{
            FontLoader.loadFont(name: filename)
            //            dispatch_once(&onceToken, { () -> Void in
            //                FontLoader.loadFont(filename)
            //            })
        }
        
        guard let font = UIFont(name: fontname, size: size) else {
            fatalError("\(fontname) not found")
        }
        return font
    }
}

private class FontLoader {
    class func loadFont(name: String) {
        //        let bundle = Bundle(forClass: FontLoader.self)
        let bundle = Bundle(for: FontLoader.self)
        let identifier = bundle.bundleIdentifier
        let fileExtension = "ttf"
        
        let url: NSURL?
        if identifier?.hasPrefix("org.cocoapods") == true {
            //            url = bundle.URLForResource(name, withExtension: fileExtension, subdirectory: "GoogleMaterialIconFont.bundle")
            url = bundle.url(forResource: name, withExtension: fileExtension, subdirectory: "GoogleMaterialIconFont.bundle")! as NSURL
        } else {
            //            url = bundle.URLForResource(name, withExtension: fileExtension)
            url = bundle.url(forResource: name, withExtension: fileExtension)! as NSURL
        }
        
        guard let fontURL = url else { fatalError("\(name) not found in bundle") }
        
        //        guard let data = NSData(contentsOfURL: fontURL),
        guard let data = NSData(contentsOf: fontURL as URL) else {return}
        guard let provider = CGDataProvider(data: data) else { return }
        let font = CGFont(provider)
        
        
        var error: Unmanaged<CFError>?
        if !CTFontManagerRegisterGraphicsFont(font!, &error) {
            let errorDescription: CFString = CFErrorCopyDescription(error!.takeUnretainedValue())
            let nsError = error!.takeUnretainedValue() as AnyObject as! NSError
            NSException(name: NSExceptionName.internalInconsistencyException, reason: errorDescription as String, userInfo: [NSUnderlyingErrorKey: nsError]).raise()
        }
    }
}
