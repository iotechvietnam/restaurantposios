//
//  Constants.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 7/25/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

let host = "http://13.67.35.142:8689"
let websocket = "ws://13.67.35.142:8686"
let urlfood = "\(host)/file?image="
let urlcate = "\(host)/icon?image="

struct AppColor {
    static let colormenutop = UIColor.init(red: 102.0/255.0, green: 7.0/255.0, blue: 39.0/255.0, alpha: 1.0)
    static let colorblack = UIColor.init(red: 109.0/255.0, green: 26.0/255.0, blue: 54.0/255.0, alpha: 1.0)// mau den xam
    static let colorred = UIColor.init(red: 152.0/255.0, green: 0.0/255.0, blue: 52.0/255.0, alpha: 1.0)// mau do cua bar
    static let colorhighlight = UIColor.init(red: 143.0/255.0, green: 168.0/255.0, blue: 0.0/255.0, alpha: 1.0)// mau xanh duoc lua chon cua button
    static let colortext = UIColor.init(red: 103.0/255.0, green: 103.0/255.0, blue: 103.0/255.0, alpha: 1.0)// mau chu
    static let colorgray = UIColor.init(red: 231.0/255.0, green: 233.0/255.0, blue: 232.0/255.0, alpha: 1.0)// mau xam trang
    static let colorpurple = UIColor.init(red: 132.0/255.0, green: 75.0/255.0, blue: 242.0/255.0, alpha: 1.0)// mau tim
}

let StatusCollection : [String : Any] = [
    "Accept" : AppColor.colormenutop,
    "Completed" : AppColor.colorhighlight,
    "Pending" : AppColor.colormenutop,
    "Booking" : AppColor.colorpurple,
    "Done" : AppColor.colortext,
    "None" : UIColor.lightGray,
    "Cancel" : UIColor.red
]

