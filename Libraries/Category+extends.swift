//
//  Category+extends.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 7/24/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

extension UIView{
    // load view from NIB
    func LoadViewFromNib(nibNameParam:String){
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibNameParam, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    // animation fly
    func animationZoom(scaleX: CGFloat, y: CGFloat) {
        self.transform = CGAffineTransform(scaleX: scaleX, y: y)
    }
    
    func animationRoted(angle : CGFloat) {
        self.transform = self.transform.rotated(by: angle)
    }
}
