//
//  Category+UIImageView.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 7/27/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

extension UIImageView{
    
    func iconMenuStyle(){
        self.layer.cornerRadius = self.frame.size.width/2
        self.layer.masksToBounds = true
    }
    
    func commonStyleImageView(){
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
    }
    
}
