//
//  Category+Math.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 9/7/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

extension Double{
    func toShow() -> String{
//        let stringbefore = String(format: "%.02f", self)
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = " "
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = "."
        numberFormatter.numberStyle = .currency
        numberFormatter.maximumFractionDigits = 2
        
        return numberFormatter.string(from: NSNumber(value: self))!
    }
}
