//
//  Category+Dictionary.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/31/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
