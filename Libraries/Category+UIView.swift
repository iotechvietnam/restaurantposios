//
//  Category+UIView.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/3/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import SwiftMessages

extension UIView{
    func CommonStyleView(){
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowOpacity = 1.0
        self.layer.cornerRadius = 10
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
    }
    
    func FlashAnimation(){
        let flash = CABasicAnimation.init(keyPath: "opacity")
        flash.fromValue = NSNumber(value: 0.0)
        flash.toValue = NSNumber(value: 1.0)
        flash.duration = 1.0;        // 1 second
        flash.autoreverses = true;    // Back
        flash.repeatCount = .infinity;       // Or whatever
        self.layer.add(flash, forKey: "flashAnimation")
    }
}

class ConfigMessage: UIView {
    
    static func show(theme:Theme, title:String, body:String){
        SwiftMessages.show {
            let view = MessageView.viewFromNib(layout: .messageView)
            // ... configure the view
            view.configureTheme(theme)
            view.configureDropShadow()
            let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
            view.configureContent(title: title, body: body, iconText: iconText)
            return view
        }
    }
}
