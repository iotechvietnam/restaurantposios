//
//  Category+UIButton.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 7/27/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import GoogleMaterialIconFont

extension UIButton{
    
    func commonStyleButton(){
        self.layer.cornerRadius = self.frame.size.width/2
        self.layer.masksToBounds = true
    }
    
    func shadowStyle(){
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSize(width: 1, height: 1)
        self.layer.shadowOpacity = 1.0//0.2
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.layer.cornerRadius).cgPath
    }
    
    func checkoutStyle(){
        self.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 50)
        self.titleEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.imageView?.contentMode = .scaleAspectFit// chinh cho hinh anh duoc chuan ko bi meo
        
        self.layer.cornerRadius = 26
        self.titleLabel?.font = UIFont(name: "Roboto", size: 17)
        self.titleLabel?.textAlignment = NSTextAlignment.left
        
        self.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
        self.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        
        // tao bong
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSize(width: 1, height: 1)
        self.layer.shadowOpacity = 1.0//0.2
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds = false
    }
    
    func addCartStyle(){
        self.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 50)
        self.titleEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.imageView?.contentMode = .scaleAspectFit// chinh cho hinh anh duoc chuan ko bi meo
        
        self.layer.cornerRadius = 30
        self.titleLabel?.font = UIFont(name: "Roboto", size: 17)
        self.titleLabel?.textAlignment = NSTextAlignment.left
        
        self.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
        self.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        
        //set background mau
        self.backgroundColor = AppColor.colorred
        self.tintColor = UIColor.white
    }
    
    func setCommonButton(name : String){
        self.setTitle(name, for: .normal)
        self.titleLabel?.font = UIFont.materialIconOfSize(size: 40)
        self.tintColor = AppColor.colorred
    }
}
