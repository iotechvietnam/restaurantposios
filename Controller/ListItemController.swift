//
//  DetailController.swift
//  SplitViewController
//
//  Created by KingCQ on 16/5/19.
//  Copyright © 2016年 KingCQ. All rights reserved.
//

import UIKit
import GoogleMaterialIconFont
import ViewAnimator// animation cho tableview va searchview
import PopupController

class ListItemController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ModeDelegate, CartDetailDelegate, TablesDelegate, ProfileDelegate, WarnningDelegate, ViewCartDelegate {
    
    func ResetUIAfterBooking() {
        resetUIAfterBooking()
    }
    
    func resetUIAfterBooking(){
        if let tablename = UserDefaults.standard.string(forKey: "tablename"){
            noticetable.text = "\(tablename) selected"
        }
        else{
            noticetable.text = "No table selected"
        }
        
        carts.removeAll()// phai remove het phan tu
        counterItem = 0 // thiet lap lai gia tri
        btnShoppingCart?.setImage(UIImage(named: "ic_cart.png"), for: .normal)// doi lai image cart
        shoppingCartNormal()// remove so luong tren cart
        // reset button add
        if collectionview != nil{
            if foods.count > 0{
                for row in 0...foods.count-1{
                    let indexpath = IndexPath(row: row, section: 0)
                    guard let cell = collectionview.cellForItem(at: indexpath) as? SettingCell else {return}
                    cell.btnFoodPlus.isUserInteractionEnabled = true
                    cell.btnFoodPlus.alpha = 1.0
                    cell.btnFoodPlus.backgroundColor = AppColor.colorred
                    cell.btnFoodPlus.isSelected = false
                }
            }
        }
        
        //reset userdefault contain data array
        UserDefaults.standard.removeObject(forKey: "foodsselected")
        
        collectionview.reloadData()
    }
    

    //warnning
    func closeWarnningController() {
        self.popup?.dismiss()
    }
    
    var viewwarnning : WarnningController!
    func acceptWarnningController() {
        self.popup?.dismiss()
        //xu ly logic nua o day
        
        //xu ly logic nua o day
        if viewwarnning?.type == "popuptable"{ // dong y chon table
            let viewmode = self.storyboard?.instantiateViewController(withIdentifier: "viewmode") as! ModeController
            viewmode.modedelegate = self
            
            self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
                
            }).didCloseHandler({ (_) in
                
            })
            
            self.popup?.show(viewmode)
        }
    }
    
    //change password
    func closeProfileController() {
        self.popup?.dismiss()
    }
    
    //tables
    func chooseModeController(table: TableObject) {
        self.popup?.dismiss()
        
        resetUIAfterBooking()
        // xoa du lieu da lua chon
        carts.removeAll()
    }
    
    func closeTablesController() {
        self.popup?.dismiss()
    }
    
    //cart detal
    func closeCartDetailController() {
        self.popup?.dismiss()
    }
    
    // mode
    func closeModeController() {
        self.popup?.dismiss()
    }
    
    
    @IBOutlet var collectionview: UICollectionView!
    
    var cateid : String?
    
    var foods : [FoodObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        LoadData()
        createCollection()
        ShowNoticeTable()
        
        createLeftBar()
        createRightBar()
        
        CollectionNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        if let data = UserDefaults.standard.object(forKey: "foodsselected") as? Data{
//            if let dataarray = NSKeyedUnarchiver.unarchiveObject(with: data) as? [FoodObject]{
//                for item in dataarray{
//                    if carts.contains(where: {food in food.foodname == item.foodname}){
//                        
//                    }
//                    else{
//                        carts.append(item)
//                    }
//                }
//                
//                print(carts.count)
//            }
//        }
    }
    
    var noticetable : UILabel!
    func ShowNoticeTable(){
        noticetable = UILabel.init(frame: CGRect(x: self.view.frame.size.width - 180, y: self.view.frame.size.height-45, width: 160, height: 40))
        noticetable.textAlignment = .center
        noticetable.textColor = UIColor.white
        noticetable.font = UIFont.systemFont(ofSize: 12.0)
        if let tablename = UserDefaults.standard.string(forKey: "tablename"){
            noticetable.text = "\(tablename) selected"
        }
        else{
            noticetable.text = "No table selected"
        }
        noticetable.layer.cornerRadius = 20
        noticetable.clipsToBounds = true
        noticetable.backgroundColor = AppColor.colorpurple
        noticetable.FlashAnimation()
        
        self.navigationController?.view.addSubview(noticetable)
    }
    
    func createLeftBar(){
        let menubutton = UIButton(type: UIButtonType.custom)
        menubutton.setTitle(String.materialIcon(font: .Menu), for: .normal)
        menubutton.titleLabel?.font = UIFont.materialIconOfSize(size: 25)
        menubutton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        
        let leftmenubar = UIBarButtonItem(customView: menubutton)
        
        self.navigationItem.leftBarButtonItem = leftmenubar
    }
    
    @objc func showMenu(){
        if splitViewController?.displayMode == UISplitViewControllerDisplayMode.primaryHidden{
            splitViewController?.preferredDisplayMode = UISplitViewControllerDisplayMode.automatic
        }
        else{
            splitViewController?.preferredDisplayMode = UISplitViewControllerDisplayMode.primaryHidden
        }
    }
    
    var btnShoppingCart : UIButton?
    var lblNumberItem : UILabel?
    var search : UIButton?// search bay gio chuyen sang thanh danh sach cac table
    var setting : UIButton?
    func createRightBar(){
        // mau cua top bar
        self.navigationController?.navigationBar.barTintColor = AppColor.colorred
        // mau cua chu tren navigation bar - la mau trang
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let rightview = UIView(frame: CGRect(x: self.view.frame.size.width - 180, y: 10, width: 180, height: 30))
        
        btnShoppingCart = UIButton(type: UIButtonType.custom)
        btnShoppingCart?.frame = CGRect(x: 0, y: 0, width: 48, height: 30)
        btnShoppingCart?.setImage(UIImage(named: "ic_cart.png"), for: .normal)
        btnShoppingCart?.tintColor = UIColor.white
        btnShoppingCart?.addTarget(self, action: #selector(showOrderCart), for: .touchUpInside)
        
        // add bagde vao button
        lblNumberItem = UILabel(frame: CGRect(x: (btnShoppingCart?.frame.size.width)!-18, y: -2, width: 18, height: 18))
        shoppingCartNormal()
        btnShoppingCart?.addSubview(lblNumberItem!)
        
        search = UIButton(type: UIButtonType.custom)
        search?.frame = CGRect(x: 90, y: 0, width: 40, height: 40)
        search?.setTitle(String.materialIcon(font: .SelectAll), for: .normal)
        search?.titleLabel?.font = UIFont.materialIconOfSize(size: 25)
        search?.addTarget(self, action: #selector(popupTable), for: .touchUpInside)
        
        setting = UIButton(type: UIButtonType.custom)
        setting?.frame = CGRect(x: 140, y: 0, width: 40, height: 40)
        setting?.setTitle(String.materialIcon(font: .MoreVert), for: .normal)
        setting?.titleLabel?.font = UIFont.materialIconOfSize(size: 25)
        setting?.addTarget(self, action: #selector(showSetting), for: .touchUpInside)
        
        rightview.addSubview(btnShoppingCart!)
        rightview.addSubview(search!)
        rightview.addSubview(setting!)
        
        let rightmenu = UIBarButtonItem(customView: rightview)
        
        self.navigationItem.rightBarButtonItem = rightmenu
    }
    
    var popup : PopupController?
    @objc func showOrderCart() {
        btnShoppingCart?.CCAnimationPop()
        
        print(carts.count)
//        let viewcart = self.storyboard?.instantiateViewController(withIdentifier: "viewcart") as! ViewCartController
//        viewcart.viewcartdelegate = self
//        viewcart.foods = carts
//
//        self.navigationController?.pushViewController(viewcart, animated: true)
        
        //check table
        if let tableid = UserDefaults.standard.string(forKey: "tableid"){
            let viewcart = self.storyboard?.instantiateViewController(withIdentifier: "viewcart") as! ViewCartController
            viewcart.viewcartdelegate = self// accept delegate for reset UI when booking and payment finish
            viewcart.foods = carts
            
            self.navigationController?.pushViewController(viewcart, animated: true)
        }
        else{// bat man hinh chon thong bao chon table
            viewwarnning = self.storyboard?.instantiateViewController(withIdentifier: "viewwarnning") as! WarnningController
            viewwarnning.warnningdelegate = self
            viewwarnning.type = "popuptable"
            
            self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
                
            }).didCloseHandler({ (_) in
                
            })
            
            self.popup?.show(viewwarnning)
        }
    }
    
    @objc func popupTable(){
        let viewmode = self.storyboard?.instantiateViewController(withIdentifier: "viewmode") as! ModeController
        viewmode.modedelegate = self
        
        self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
            
        }).didCloseHandler({ (_) in
            
        })
        
        self.popup?.show(viewmode)
        
//        btnShoppingCart?.isUserInteractionEnabled = false
    }
    
    var textfield : UITextField?
    var issearch : Bool = false
    @objc func searchOrder(){
        search?.CCAnimationPop()
        
        if issearch == false{
            textfield = UITextField(frame: CGRect(x: self.view.frame.size.width/2-200, y: 0, width: 400, height: 40))
            textfield?.backgroundColor = UIColor.white
            textfield?.becomeFirstResponder()
            textfield?.layer.cornerRadius = 10
            self.navigationController?.navigationBar.addSubview(textfield!)
            
            let animation = AnimationType.zoom(scale: 0.0)
            textfield?.animate(animations: [animation])
            
            issearch = true
        }
        else{
            let animation = AnimationType.zoom(scale: 1.0)
            textfield?.animate(animations: [animation], reversed: true, initialAlpha: 1.0, finalAlpha: 0.0, delay: 0.0, duration: 0.5, completion: {
                self.textfield?.removeFromSuperview()
            })
            
            issearch = false
        }
    }

    let namemenu : [String] = ["Tables","Change Password","Logout"]
    let imagemenu : [String] = ["ic_tables","ic_key","ic_logout"]
    
    @objc func showSetting(){
        setting?.CCAnimationPop()
        
        FTConfiguration.shared.menuRowHeight = 50
        FTConfiguration.shared.menuWidth = 200
        FTConfiguration.shared.textColor = UIColor(red: 114.0/255.0, green: 114.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        FTConfiguration.shared.backgoundTintColor = UIColor.white
        FTPopOverMenu.showForSender(sender: setting!, with: namemenu, menuImageArray: imagemenu, done: { (selectedIndex) -> () in
            // chon index
            if selectedIndex == 0{// table
                let viewtable = self.storyboard?.instantiateViewController(withIdentifier: "viewtable") as! TablesController
                viewtable.tabledelegate = self

                self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
                    
                }).didCloseHandler({ (_) in
                    
                })
                
                self.popup?.show(viewtable)
            }
            else if selectedIndex == 1{// change password
                let viewprofile = self.storyboard?.instantiateViewController(withIdentifier: "viewprofile") as! ProfileController
                viewprofile.profiledelegate = self
                
                self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in

                }).didCloseHandler({ (_) in

                })

                self.popup?.show(viewprofile)
            }
            else if selectedIndex == 2{// logout
                // logout thoi
                let viewwarnning = self.storyboard?.instantiateViewController(withIdentifier: "viewwarnning") as! WarnningController
                viewwarnning.warnningdelegate = self
                viewwarnning.type = "warnning"
                
                self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
                    
                }).didCloseHandler({ (_) in
                    
                })
                
                self.popup?.show(viewwarnning)
            }
        }) {
            // huy bo
        }
    }
    
    func CollectionNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(FoodDataDynamic(notice:)), name: Notification.Name("FoodDataDynamic"), object: nil)
    }
    
    @objc func FoodDataDynamic(notice : Notification){
        foods = notice.object as! [FoodObject]
        
        collectionview.reloadData()
    }
    
    func createCollection(){
        collectionview.delegate = self
        collectionview.dataSource = self
        
        // bo image scroll cua collection
        collectionview.showsVerticalScrollIndicator = false
        collectionview.showsHorizontalScrollIndicator = false
        
        // khoang cach giua cac cell
        collectionview.contentInset = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
        
        // background nen cua ca collection
        collectionview.backgroundColor = AppColor.colorgray
    }
    
    func LoadData(){
        FoodObject.GetFoodDataWithID(cateid: cateid!) { (foods) in
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: Notification.Name("FoodDataDynamic"), object: foods)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return foods.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemcell", for: indexPath as IndexPath) as! CollectionCell
        
        if foods.count > 0{
            if foods[indexPath.row].foodimages.count > 0{
                if let urlhttp = UserDefaults.standard.string(forKey: "http"){
                    cell.imageItem.af_setImage(withURL: URL(string: "\(urlhttp)/file?image=\(foods[indexPath.row].foodimages[0])")!)
                }
            }
            cell.titleitem.text = foods[indexPath.row].foodname
            cell.priceItem.text = "\(foods[indexPath.row].foodprice.toShow())"
            cell.discountItem.text = "\(foods[indexPath.row].fooddiscount.toShow())"
            cell.setStyle()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let viewdetail = self.storyboard?.instantiateViewController(withIdentifier: "cartdetail") as! CartDetailController
        viewdetail.cartdetaildelegate = self
        viewdetail.food = foods[indexPath.row]
        
        self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
            
        }).didCloseHandler({ (_) in
            
        })
        
        self.popup?.show(viewdetail)
    }
    
    // xac dinh so san pham xuat hien tren 1 dong
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSize = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)-60) / 3
        return CGSize(width: itemSize, height: itemSize*2/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    
    // ANIMATION ADD PLUS
    var carts : [FoodObject] = [] // day thuc chat la danh sach mon an duoc lua chon boi khach hang
    var counterItem = 0
    @IBAction func actAddItemToCart(_ sender: UIButton) {
        
        sender.CCAnimationPop()
        
//        sender.backgroundColor = AppColor.colorhighlight
//
//        counterItem += 1
//        btnShoppingCart?.CCAnimationPop()
//        btnShoppingCart?.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
//        btnShoppingCart?.setImage(UIImage(named: "ic_cart_hi.png"), for: .normal)
//        btnShoppingCart?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 10)
//        btnShoppingCart?.titleLabel?.textAlignment = .center
//
//        shoppingCartHighlight()
//        lblNumberItem?.text = "\(counterItem)"
//        // mau cua text
//        btnShoppingCart?.setTitleColor(UIColor.white, for: .normal)
//        // mau cua image
//        btnShoppingCart?.tintColor = AppColor.colorhighlight
        if sender.isSelected == false{
            
            sender.isSelected = true
            
            sender.backgroundColor = AppColor.colorhighlight
            sender.setTitleColor(UIColor.white, for: .selected)
            
            counterItem += 1
            shoppingCartHighlight()
            
            // mau cua text
            btnShoppingCart?.setTitleColor(UIColor.white, for: .normal)
            // mau cua image
            btnShoppingCart?.tintColor = AppColor.colorhighlight
            
            // them gia tri vao mang
            let buttonposition = sender.convert(CGPoint.zero, to: collectionview)
            let indexpath = collectionview.indexPathForItem(at: buttonposition)
            carts.append(foods[(indexpath?.row)!])
            
        }
        else{
            
            sender.isSelected = false
            
            sender.backgroundColor = AppColor.colorred
            
            counterItem -= 1
            if counterItem == 0{
                shoppingCartNormal()
            }
            else{
                shoppingCartHighlight()
            }
            
            // mau cua text
            btnShoppingCart?.setTitleColor(UIColor.white, for: .normal)
            // mau cua image
            btnShoppingCart?.tintColor = AppColor.colorhighlight
            
            // them gia tri vao mang
            let buttonposition = sender.convert(CGPoint.zero, to: collectionview)
            let indexpath = collectionview.indexPathForItem(at: buttonposition)
            if let index = carts.index(of: foods[(indexpath?.row)!]){
                carts.remove(at: index)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        // save mang foods duoc chon xuong userdefault
        if let dataarray = NSKeyedArchiver.archivedData(withRootObject: carts) as? Data{
            UserDefaults.standard.set(dataarray, forKey: "foodsselected")
            UserDefaults.standard.synchronize()
        }
        // su kien khi roi khoi controller nay
//        let animation = AnimationType.zoom(scale: 1.0)
//        //            textfield?.animate(animations: [animation])
//        textfield?.animate(animations: [animation], reversed: true, initialAlpha: 1.0, finalAlpha: 0.0, delay: 0.0, duration: 0.5, completion: {
//            self.textfield?.removeFromSuperview()
//        })
        
//        issearch = false
        
        // reset gio hang
//        counterItem = 0 // thiet lap lai gia tri
//        btnShoppingCart?.setImage(UIImage(named: "ic_cart.png"), for: .normal)// doi lai image cart
//        shoppingCartNormal()// remove so luong tren cart
//        // reset button add
//        if collectionview != nil{
//            if foods.count > 0{
//                for row in 0...foods.count-1{
//                    let indexpath = IndexPath(row: row, section: 0)
//                    guard let cell = collectionview.cellForItem(at: indexpath) as? CollectionCell else {return}
//                    cell.btnPlus.isUserInteractionEnabled = true
//                    cell.btnPlus.alpha = 1.0
//                    cell.btnPlus.backgroundColor = AppColor.colorred
//                }
//            }
//        }
    }
    
//    func shoppingCartNormal(){
//        lblNumberItem?.backgroundColor = UIColor.clear
//        lblNumberItem?.textColor = UIColor.clear
//        lblNumberItem?.textAlignment = .center
//        lblNumberItem?.layer.cornerRadius = (lblNumberItem?.frame.size.width)!/2
//        lblNumberItem?.layer.masksToBounds = true
//        lblNumberItem?.font = UIFont.systemFont(ofSize: 0.0)
//    }
//
//    func shoppingCartHighlight(){
//        lblNumberItem?.backgroundColor = AppColor.colorhighlight
//        lblNumberItem?.textColor = UIColor.white
//        lblNumberItem?.textAlignment = .center
//        lblNumberItem?.layer.cornerRadius = (lblNumberItem?.frame.size.width)!/2
//        lblNumberItem?.layer.masksToBounds = true
//        lblNumberItem?.font = UIFont.systemFont(ofSize: 10.0)
//    }
    func shoppingCartNormal(){
        btnShoppingCart?.CCAnimationPop()
        btnShoppingCart?.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        btnShoppingCart?.setImage(UIImage(named: "ic_cart.png"), for: .normal)
        btnShoppingCart?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 10)
        btnShoppingCart?.titleLabel?.textAlignment = .center
        
        lblNumberItem?.backgroundColor = UIColor.clear
        lblNumberItem?.textColor = UIColor.clear
        lblNumberItem?.textAlignment = .center
        lblNumberItem?.layer.cornerRadius = (lblNumberItem?.frame.size.width)!/2
        lblNumberItem?.layer.masksToBounds = true
        lblNumberItem?.font = UIFont.systemFont(ofSize: 0.0)
    }
    
    func shoppingCartHighlight(){
        btnShoppingCart?.CCAnimationPop()
        btnShoppingCart?.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        btnShoppingCart?.setImage(UIImage(named: "ic_cart_hi.png"), for: .normal)
        btnShoppingCart?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 10)
        btnShoppingCart?.titleLabel?.textAlignment = .center
        
        
        lblNumberItem?.text = "\(counterItem)"
        // mau cua text
        btnShoppingCart?.setTitleColor(UIColor.white, for: .normal)
        // mau cua image
        btnShoppingCart?.tintColor = AppColor.colorhighlight
        
        lblNumberItem?.backgroundColor = AppColor.colorhighlight
        lblNumberItem?.textColor = UIColor.white
        lblNumberItem?.textAlignment = .center
        lblNumberItem?.layer.cornerRadius = (lblNumberItem?.frame.size.width)!/2
        lblNumberItem?.layer.masksToBounds = true
        lblNumberItem?.font = UIFont.systemFont(ofSize: 10.0)
    }
}
