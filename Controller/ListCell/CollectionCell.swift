//
//  CollectionCell.swift
//  Restaurant4POS
//
//  Created by ltk1205 on 7/30/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import GoogleMaterialIconFont

class CollectionCell: UICollectionViewCell {
    
    @IBOutlet var imageItem: UIImageView!
    @IBOutlet var titleitem: UILabel!
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet var priceItem: UILabel!
    @IBOutlet var discountItem: UILabel!
    
    func setStyle(){
        self.contentView.layer.cornerRadius = 10
        self.contentView.layer.masksToBounds = true
        
        self.priceItem.font = UIFont.boldSystemFont(ofSize: 17)
        
        btnPlus.commonStyleButton()
        btnPlus.titleLabel?.font = UIFont.materialIconOfSize(size: btnPlus.frame.size.width-20)
        btnPlus.setTitle(String.materialIcon(font: MaterialIconFont.Add), for: .normal)
        btnPlus.backgroundColor = AppColor.colorred
        btnPlus.setTitleColor(UIColor.white, for: .normal)
        btnPlus.shadowStyle()
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSize(width: 1, height: 1)
        self.layer.shadowOpacity = 1.0//0.2
                
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.contentView.layer.cornerRadius).cgPath
    }
}
