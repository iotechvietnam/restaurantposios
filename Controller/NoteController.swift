//
//  NoteController.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 9/14/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import PopupController

protocol NoteDelegate : class {
    func RequestNote(notes : [String], index : Int)
}

class NoteController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, PopupContentViewController, UICollectionViewDelegateFlowLayout {

    weak var notedelegate : NoteDelegate?
    
    var noteexist : String = ""
    var arraynoteexist : [String] = []
    var indexoforder : Int = 0
    
    @IBOutlet var notecollection: UICollectionView!
    @IBOutlet var btnsave: UIButton!
    @IBAction func actSaveNote(_ sender: UIButton) {
        notedelegate?.RequestNote(notes: arraynoteexist, index: indexoforder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        LoadInterface()
        //load data
        NoteObject.GetListNotes { (notes) in
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: Notification.Name("LoadNotesSuccess"), object: notes)
            }
        }
        
        arraynoteexist = noteexist.components(separatedBy: ",")
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoadNotesSuccess(notice:)), name: Notification.Name("LoadNotesSuccess"), object: nil)
    }
    
    @objc func LoadNotesSuccess(notice : Notification){
        notes = notice.object as! [NoteObject]
        
        notecollection.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 400, height: 450)
    }
    
    //=============== COLLECTION ==========
    var notes : [NoteObject] = []
    var notesexist : [NoteObject] = []
    func LoadInterface(){
        notecollection.delegate = self
        notecollection.dataSource = self
        
        // bo image scroll cua collection
        notecollection.showsVerticalScrollIndicator = false
        notecollection.showsHorizontalScrollIndicator = false
        
        // khoang cach giua cac cell
        notecollection.contentInset = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
        
        // background nen cua ca collection
        notecollection.backgroundColor = AppColor.colorgray
        
        btnsave.setTitle("Request".uppercased(), for: .normal)
        btnsave.backgroundColor = AppColor.colorred
        btnsave.setTitleColor(UIColor.white, for: .normal)// mau cua text
        btnsave.layer.cornerRadius = 20
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "modecell", for: indexPath as IndexPath) as! ModeCell
        
        cell.tablename.text = notes[indexPath.row].notename
        cell.setStyleNormal()
        
        for index in 0...arraynoteexist.count - 1{
            if arraynoteexist[index] == notes[indexPath.row].notename{
                cell.setStyleSelected()
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "modecell", for: indexPath as IndexPath) as! ModeCell
        
//        for noteobj in notesexist{
//            if noteobj.notename == notes[indexPath.row].notename{
//                if let index = notesexist.index(where: {$0.notename == notes[indexPath.row].notename}){
//                    notesexist.remove(at: index)
//                }
//                cell.setStyleNormal()
//            }
//            else{
//                noteexist.append(notes[indexPath.row].notename)
//                cell.setStyleSelected()
//            }
//        }
        if arraynoteexist.contains(notes[indexPath.row].notename){
            let index = arraynoteexist.index(where : { $0 == notes[indexPath.row].notename })
            arraynoteexist.remove(at: index!)
            cell.setStyleNormal()
        }
        else{
            arraynoteexist.append(notes[indexPath.row].notename)
            cell.setStyleSelected()
        }
        
        notecollection.reloadData()
    }
    
    // xac dinh so san pham xuat hien tren 1 dong
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSize = (notecollection.frame.width - (notecollection.contentInset.left + notecollection.contentInset.right)-60) / 2
        return CGSize(width: itemSize, height: itemSize*2/4)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
