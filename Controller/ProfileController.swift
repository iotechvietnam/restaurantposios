//
//  ProfileController.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/14/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import PopupController

protocol ProfileDelegate : class {
    func closeProfileController()
}

class ProfileController: UIViewController, PopupContentViewController {

    weak var profiledelegate : ProfileDelegate?
    
    @IBOutlet var headerprofile: UILabel!
    @IBOutlet var fullname: SkyFloatingLabelTextField!
    @IBOutlet var password: SkyFloatingLabelTextField!
    @IBOutlet var repassword: SkyFloatingLabelTextField!
    @IBOutlet var btncancel: UIButton!
    @IBAction func actCancel(_ sender: UIButton) {
        profiledelegate?.closeProfileController()
    }
    @IBOutlet var btnsave: UIButton!
    @IBAction func actSave(_ sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.frame = CGRect(x: 0, y: 0, width: 400, height: 450)
        
        LoadInterface()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 400, height: 450)
    }
    
    var textFields: [SkyFloatingLabelTextField] = []
    func LoadInterface(){
        headerprofile.text = "Change Password"
        
        fullname.placeholder = "Full name"
        password.placeholder = "Password"
        repassword.placeholder = "Re-enter Password"
        
        btnsave.setTitle("Save".uppercased(), for: .normal)
        btnsave.backgroundColor = AppColor.colorred
        btnsave.setTitleColor(UIColor.white, for: .normal)// mau cua text
        btnsave.layer.cornerRadius = 20
        
        if btncancel != nil{
            btncancel.setTitle("Cancel".uppercased(), for: .normal)
            btncancel.backgroundColor = UIColor.white
            btncancel.setTitleColor(AppColor.colorred, for: .normal)// mau cua text
            btncancel.layer.cornerRadius = 20
            btncancel.layer.borderWidth = 1
            btncancel.layer.borderColor = AppColor.colorred.cgColor
        }
    }
}
