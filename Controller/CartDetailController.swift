//
//  CartDetailController.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/10/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import PopupController
import GoogleMaterialIconFont

protocol CartDetailDelegate : class {
    func closeCartDetailController()
}

class CartDetailController: UIViewController, PopupContentViewController {
    
    weak var cartdetaildelegate : CartDetailDelegate?
    
    @IBOutlet var headerdetail: UILabel!
    @IBOutlet var btnclose: UIButton!
    @IBAction func actClose(_ sender: UIButton) {
        cartdetaildelegate?.closeCartDetailController()
    }
    @IBOutlet var btnaddcart: UIButton!
    @IBAction func actAddCart(_ sender: UIButton) {
    }
    
    @IBOutlet var total: UILabel!
    @IBOutlet var discount: UILabel!
    
    @IBOutlet var quantity: UILabel!
    @IBOutlet var btnsub: UIButton!
    @IBAction func actSub(_ sender: UIButton) {
        btnsub.CCAnimationPop()
        quantity.CCAnimationPop()
        
        guard let tmp = Int(quantity.text!) else {return}
        var number = tmp
        if number > 1{
            number = number - 1
        }
        else{
            number = 1
        }
        quantity.text = String(describing: number)
        
        UpdatePrice(quality: number)
        
    }
    @IBOutlet var btnplus: UIButton!
    @IBAction func actPlus(_ sender: UIButton) {
        btnplus.CCAnimationPop()
        quantity.CCAnimationPop()
        
        guard let tmp = Int(quantity.text!) else {return}
        var number = tmp
        number = number + 1
        quantity.text = String(describing: number)
        
        UpdatePrice(quality: number)
    }
    
    @IBOutlet var orderdescription: UITextView!
    
    func UpdatePrice(quality : Int){
        let sum = Double(food.foodprice) * Double(quality)
        total.text = "\(sum.toShow())"
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 800, height: 600)
    }
    

    var food : FoodObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame = CGRect(x: 0, y: 0, width: 800, height: 600)
        
        LoadInterface()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func LoadInterface(){
        
        headerdetail.text = food.foodname
        
        if btnclose != nil{
            btnclose.setCommonButton(name: String.materialIcon(font: .Close))
        }
        
        let price = Double(food.foodprice)
        let numofitems = (quantity.text! as NSString).doubleValue
        let sum = price*numofitems
        total.text = "$ \(sum.toShow())"
        let saleoff = String(describing: food.fooddiscount.toShow())
        discount.text = saleoff
        
        quantity.text = "1"
        btnsub.setCommonButton(name: String.materialIcon(font: .RemoveCircle))
        btnplus.setCommonButton(name: String.materialIcon(font: .AddCircle))
        btnaddcart.addCartStyle()
        btnaddcart.setTitle("Add To Cart".uppercased(), for: .normal)
        btnaddcart.setImage(UIImage.init(named: "ic_cart.png"), for: .normal)
//        btnaddcart
        
        orderdescription.text = food.fooddescription
        orderdescription.isEditable = false
        
        SetSlideShow()
    }
    
    @IBOutlet var slideshow: ImageSlideshow!
    func SetSlideShow(){
        slideshow.slideshowInterval = 5.0
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        pageControl.pageIndicatorTintColor = UIColor.black
        slideshow.pageIndicator = pageControl
        
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
            print("current page:", page)
        }
        
        if (food.foodimages.count) > 0{
            var slidesources : [AlamofireSource] = []
            for imagename in (food.foodimages){
                if let urlhttp = UserDefaults.standard.string(forKey: "http"){
                    let slidesource = AlamofireSource(url: URL(string: "\(urlhttp)/file?image=\(imagename)")!)
                    
                    slidesources.append(slidesource)
                }
            }
            
            slideshow.setImageInputs(slidesources)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if slideshow != nil{
            slideshow.removeFromSuperview()
            slideshow = nil
        }
    }
}
