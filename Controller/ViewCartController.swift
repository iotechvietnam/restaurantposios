//
//  ViewCartController.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/3/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import GoogleMaterialIconFont
import PopupController
import NVActivityIndicatorView
import SwipyCell

import RxStarscream
import Starscream

protocol ViewCartDelegate : class {
    func ResetUIAfterBooking()
}

class ViewCartController: UIViewController, CheckOutViewDelegate, UITableViewDataSource, UITableViewDelegate, WarnningDelegate, NoteDelegate {
    
    weak var viewcartdelegate : ViewCartDelegate?
    
    func closeWarnningController() {
        self.popup?.dismiss()
    }
    
    func acceptWarnningController(){//acceptWarnningControllerLoadListTable() {
        self.popup?.dismiss()
        if viewwarnning?.type == "bookingcart"{ // dong y booking
            if foods.count > 0{
                
                ACProgressHUD.shared.progressText = "Processing a booking"
                ACProgressHUD.shared.showHUD()
                
                if let tableid = UserDefaults.standard.string(forKey: "tableid"){
                    cart.carttable.tableid = tableid
                }
                if let tablename = UserDefaults.standard.string(forKey: "tablename"){
                    cart.carttable.tablename = tablename
                }
                if cart.cartid == ""{// first create cart
                    FoodObject.BookingOrder(cart: cart) { (notice) in
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name: Notification.Name("LoadNewCartInserted"), object: notice)
                        }
                    }
                }
                else{// update cart
                    FoodObject.UpdateBookingOrder(cart: cart) { (message1) in
                        if message1 == "Success"{
                            self.isloadnewcart = false
                            
                            self.foods.removeAll()
                            
                            DispatchQueue.main.async {
                                NotificationCenter.default.post(name: Notification.Name("UpdatedCartExists"), object: message1)
                            }
                        }
                    }
                }
                // delete array foods and re updated UI in home controller
                self.viewcartdelegate?.ResetUIAfterBooking()
            }
        }
        else if viewwarnning?.type == "paymentcart"{// dong y thanh toan
            paymentLoadding()
        }
        else if viewwarnning?.type == "clearcart"{// dong y clear cart
            CartObject.ClearABooking(cart: carts) { (message) in
                if message == "Success"{// da remove order thanh cong
                    
                }
                else{ // khong the remove duoc order
                    
                }
            }
        }
        else if viewwarnning?.type == "paymentsuccess"{
            self.navigationController?.popToRootViewController(animated: true)
        }
        else if viewwarnning?.type == "nodata"{// khong co so lieu
            self.navigationController?.popViewController(animated: true)// quay tro ve man hinh chon
            self.popup?.dismiss()// dong popup
        }
        else if viewwarnning?.type == "payementfailure"{
            self.navigationController?.popViewController(animated: true)
        }
        else if viewwarnning?.type == ""{
            
        }
    }
    
    var viewloadding : UIView?
    var viewpayment : PaymentLoadding?
    func paymentLoadding(){
        //============== VIEW BEN TRAI ============//
        // thay giao dien tableview bang man hinh loading va thong bao
        viewloadding = UIView(frame: CGRect(x: headercart.frame.origin.x, y: headercart.frame.origin.y, width: (tableview?.frame.size.width)!, height: (tableview?.frame.size.height)!+headercart.frame.size.height))
        viewloadding?.backgroundColor = UIColor.white
        viewloadding?.CommonStyleView()
        viewloadding?.layer.cornerRadius = 0// khong bo goc 
        // chuyen giao dien loadding
        enabledLoadding()
        containercart.addSubview(viewloadding!)
        // them loaddingview vao
        let frameloadding : CGRect = CGRect(x: containercart.frame.size.width*2/5, y: containercart.frame.size.height/6, width: containercart.frame.size.width/5, height: containercart.frame.size.width/5)
        let loadding = NVActivityIndicatorView(frame: frameloadding, type: NVActivityIndicatorType?.none, color: AppColor.colorred, padding: 10)
        // Load 3s
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
//            self.viewwarnning = self.storyboard?.instantiateViewController(withIdentifier: "viewwarnning") as! WarnningController
//            self.viewwarnning.warnningdelegate = self
//            self.viewwarnning.type = "paymentsuccess"
//
//            self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
//
//            }).didCloseHandler({ (_) in
//
//            })
//
//            self.popup?.show(self.viewwarnning)
//        }
        CartObject.PaymentABooking(cart: carts) { (message) in
            if message == "Success"{ // payment success
                self.viewwarnning = self.storyboard?.instantiateViewController(withIdentifier: "viewwarnning") as! WarnningController
                self.viewwarnning.warnningdelegate = self
                self.viewwarnning.type = "paymentsuccess"
                
                self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
                    
                }).didCloseHandler({ (_) in
                    
                })
                
                self.popup?.show(self.viewwarnning)
            }
            else{ // payment failure
                self.viewwarnning = self.storyboard?.instantiateViewController(withIdentifier: "viewwarnning") as! WarnningController
                self.viewwarnning.warnningdelegate = self
                self.viewwarnning.type = "payementfailure"
                
                self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
                    
                }).didCloseHandler({ (_) in
                    
                })
                
                self.popup?.show(self.viewwarnning)
            }
        }
        loadding.startAnimating()
        
        viewloadding?.addSubview(loadding)
        //them text thong bao
        let frameheadertext = CGRect(x: containercart.frame.size.width/3, y: containerorder.frame.size.width/2, width: containercart.frame.size.width/3, height: 30)
        let headertext = UILabel(frame: frameheadertext)
        headertext.text = "Thank for purchased"
        headertext.textAlignment = .center
        headertext.font = UIFont.boldSystemFont(ofSize: 17)
        viewloadding?.addSubview(headertext)
        // them noi dung thong bao
        let framecontenttext = CGRect(x: containercart.frame.size.width/4, y: containerorder.frame.size.width/2+headertext.frame.size.height, width: containercart.frame.size.width/2, height: 50)
        let contenttext = UILabel(frame: framecontenttext)
        contenttext.text = "We'll start processing your order as soon as your payment has been confirmed."
        contenttext.textAlignment = .center
        contenttext.font = UIFont.init(name: "Roboto", size: 15)
        contenttext.textColor = UIColor.lightGray
        contenttext.lineBreakMode = .byWordWrapping
        contenttext.numberOfLines = 0
        viewloadding?.addSubview(contenttext)
        
        //=============== VIEW CHECKOUT =============//
        viewpayment = PaymentLoadding(frame: CGRect(x: checkoutview.frame.origin.x, y: checkoutview.frame.origin.y-20, width: checkoutview.frame.size.width, height: checkoutview.frame.size.height+20))
        viewpayment?.backgroundColor = UIColor.white
        viewpayment?.CommonStyleView()
        containercheckout.addSubview(viewpayment!)
        
        //link den file NIB
        viewpayment?.LoadViewFromNib(nibNameParam: "PaymentLoadding")
        viewpayment?.loadInterface()
        
        // xu ly du lieu o day
    }
    
    func enabledOrder(){
        // an tableview
        headercart.isHidden = false
        tableview?.isHidden = false
        containerorder.isHidden = false
        // hien man hinh loadding
        viewloadding?.isHidden = true
        
        //an du lieu checkout
        checkoutview.isHidden = false
    }
    
    func enabledLoadding(){
        // an tableview
        headercart.isHidden = true
        tableview?.isHidden = true
        containerorder.isHidden = true
        // hien man hinh loadding
        viewloadding?.isHidden = false
        
        //an du lieu checkout
        checkoutview.isHidden = true
    }
    
    func CollectionNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(LoadNewCartInserted(notice:)), name: Notification.Name("LoadNewCartInserted"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdatedCartExists(notice:)), name: Notification.Name("UpdatedCartExists"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(GetAllOrderSuccess(notice:)), name: Notification.Name("GetAllOrderSuccess"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateRealTimeStatusCart(notice:)), name: Notification.Name("UpdateRealTimeStatusCart"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(GetAllOrderFailure(notice:)), name: Notification.Name("GetAllOrderFailure"), object: nil)
    }
    
    @objc func GetAllOrderSuccess(notice : Notification){
        carts = notice.object as! CartObject
        
        if isloadnewcart == false{// trong truong hop update cart va load lai cart controller
            foods.removeAll()
            stupids.removeAll()
        }
        else{// khi lan dau vao man hinh cart controller
            //        if isloadnewcart == true{
            // tinh toan gia tri subtotal va total
            if carts.cartorder.count > 0{
                if let vat : Double = UserDefaults.standard.double(forKey: "VAT"){
                    for row in 0...carts.cartorder.count-1{
                        let indexpath = IndexPath(row: row, section: 0)
                        
                        subtotal += carts.cartorder[indexpath.row].foodprice*carts.cartorder[indexpath.row].foodquantity
                        taxvalue += carts.cartorder[indexpath.row].foodprice*carts.cartorder[indexpath.row].foodquantity*vat/100
                        total = subtotal + taxvalue
                    }
                }
            }
        }
        
        // them vao mang show
        if carts.cartorder.count > 0{
            for cartitem in carts.cartorder{
                print("name is : \(cartitem.foodname)")
                let stupid = StupidCartObject.init()
                stupid.stupidname = cartitem.foodname
                stupid.stupidquantity = Int(cartitem.foodquantity)
                stupid.stupidprice = cartitem.foodprice
                stupid.stupidstatus = cartitem.foodstatus
                stupid.isbooking = true
//                stupid.stupidstatus = "Pending"
                
                stupids.append(stupid)
            }
            print("so luong them la \(stupids.count)")
        }
        
        cart.cartid = carts.cartid
        
        if foods.count + carts.cartorder.count > 0{// neu co du lieu
            LoadListOrder()// danh sach order
        }
        else{ // khong co du lieu thong bao va quit ra ngoai
            viewwarnning = self.storyboard?.instantiateViewController(withIdentifier: "viewwarnning") as! WarnningController
            viewwarnning.warnningdelegate = self
            viewwarnning.type = "nodata"
            
            self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
                
            }).didCloseHandler({ (_) in
                
            })
            
            self.popup?.show(viewwarnning)
        }
        
        tableview?.reloadData()
        
        ACProgressHUD.shared.hideHUD()
    }
    
    @objc func UpdateRealTimeStatusCart(notice : Notification){
        let cartobject = notice.object as! CartObject
        
        // remove element in stupids array include isbooking = false
        stupids.removeAll()
        
        if cartobject.cartorder.count > 0{
            ParseExistDataStupid()
            for cartitem in cartobject.cartorder{
                print("name is : \(cartitem.foodname)")
                let stupid = StupidCartObject.init()
                stupid.stupidname = cartitem.foodname
                stupid.stupidquantity = Int(cartitem.foodquantity)
                stupid.stupidprice = cartitem.foodprice
                stupid.stupidstatus = cartitem.foodstatus
                stupid.isbooking = true
                
                stupids.append(stupid)
            }
        }
        
        tableview?.reloadData()
        
        ACProgressHUD.shared.hideHUD()
    }
    
    @objc func GetAllOrderFailure(notice : Notification){
        
        // cha lam gi voi doi tuong carts
        if isloadnewcart == false{
            foods.removeAll()
        }
        
        LoadListOrder()
        
        tableview?.reloadData()
        ACProgressHUD.shared.hideHUD()
    }
    
    
    
    func LoadFirstRealTimeCart(){
        print("\(UserDefaults.standard.string(forKey: "tableid"))")
        if let tableid = UserDefaults.standard.string(forKey: "tableid"){
            let socket = WebSocket(url: URL(string: "ws://13.67.35.142:8686/cart?id=\(tableid)")!)
            socket.connect()
            
            socket.rx.connected.subscribe(onNext: { (isconnected) in
                socket.rx.text.subscribe(onNext: { (jsondata) in
                    
                    CategoryObject.IsOperationRealtimeData(responseString: jsondata, callback: { (realtime) in
                        if (realtime == "get" || realtime == ""){
                            CartObject.GetAllCart(responseString: jsondata, success: { (cartobject) in
                                DispatchQueue.main.async {
                                    NotificationCenter.default.post(name: Notification.Name("GetAllOrderSuccess"), object: cartobject)
                                }
                            }, failure: { (mess) in
                                DispatchQueue.main.async {
                                    NotificationCenter.default.post(name: Notification.Name("GetAllOrderFailure"), object: mess)
                                }
                            })
                        }
                        else if realtime == "update"{
                            CartObject.IsRealtimeInSelfTable(responseString: jsondata, callback: { (id) in
                                if tableid == id{// change realtime data in this table -> recall data
                                    CartObject.GetAllCart(responseString: jsondata, success: { (listcart) in
                                        DispatchQueue.main.async {
                                            NotificationCenter.default.post(name: Notification.Name("UpdateRealTimeStatusCart"), object: listcart)
                                        }
                                    }, failure: { (mess) in
                                        DispatchQueue.main.async {
                                            NotificationCenter.default.post(name: Notification.Name("GetAllOrderFailure"), object: mess)
                                        }
                                    })
                                }
                                else{// change realtime data in other table -> no any more action
                                    
                                }
                            })
                        }
                    })
                    
                    
                }, onError: { (err) in
                    print("Error!!!")
                }, onCompleted: {
                    print("Completed")
                }) {
                    print("Disposed")
                }
            }, onError: { (error) in
                print("Error")
            }, onCompleted: {
                print("completed")
            }) {
                print("Disposed")
            }
        }
        else{
            
        }
    }
    
    @objc func UpdatedCartExists(notice: Notification){
        
        isloadnewcart = false
    }
    
    @objc func LoadNewCartInserted(notice : Notification){
        print("\(UserDefaults.standard.string(forKey: "tableid"))")
        
        isloadnewcart = false
        
        let message = notice.object as! String
        
        if message == "Success"{
            // goi den phan realtime data
            let socket = WebSocket(url: URL(string: "ws://13.67.35.142:8686/cart?id=\(cart.carttable.tableid)")!)
            socket.connect()
            
            socket.rx.connected.subscribe(onNext: { (isconnected) in
                socket.rx.text.subscribe(onNext: { (jsondata) in
                    
                    CategoryObject.IsOperationRealtimeData(responseString: jsondata, callback: { (realtime) in
                        if realtime == "get"{// load data them moi
                            CartObject.GetAllCart(responseString: jsondata, success: { (cartobject) in
                                DispatchQueue.main.async {
                                    NotificationCenter.default.post(name: Notification.Name("GetAllOrderSuccess"), object: cartobject)
                                }
                            }, failure: { (mess) in
                                DispatchQueue.main.async {
                                    NotificationCenter.default.post(name: Notification.Name("GetAllOrderFailure"), object: mess)
                                }
                            })
                        }
                        else{// load data update
                            
                        }
                    })
                    
                }, onError: { (err) in
                    print("Error!!!")
                }, onCompleted: {
                    print("Completed")
                }) {
                    print("Disposed")
                }
            }, onError: { (error) in
                print("Error")
            }, onCompleted: {
                print("completed")
            }) {
                print("Disposed")
            }
        }
        else{// insert cart khong thanh cong -> thong bao cho khach hang biet
            
        }
        
    }
    
    func closeModeController() {
        self.popup?.dismiss()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        createLeftBar()// left menu
        createRightBar()
        
        CollectionNotification()
        
        ParseExistDataStupid()
        print("foods : \(foods.count), carts : \(carts.cartorder.count), stupids : \(stupids.count)")
        if isloadnewcart == true{// Chi goi data tu server
            LoadFirstRealTimeCart()
        }
    }
    
    var stupids : [StupidCartObject] = []
    func ParseExistDataStupid(){
        
        for food in foods{
            let stupid = StupidCartObject.init()
            
            stupid.stupidname = food.foodname
            stupid.stupidquantity = 1
            stupid.stupidprice = food.foodprice
            stupid.stupidstatus = "None"
            stupid.isbooking = false
            stupid.stupidnote = ""
            
            stupids.append(stupid)
        }
        
        calcTotalBooking()
    }
    
    func calcTotalBooking(){
        // tinh toan gia tri subtotal va total
        if foods.count > 0{
            for rows in 0...foods.count-1{
                let indexpath = IndexPath(row: rows, section: 0)
                
                subtotal += foods[indexpath.row].foodprice*1
                taxvalue += foods[indexpath.row].foodprice*1*0.1
                total = subtotal + taxvalue
            }
        }
    }
    
    func calcRenewSubTotal(food : StupidCartObject){
        subtotal -= food.stupidprice
        taxvalue -= food.stupidprice*0.1
        total = subtotal + taxvalue
        print("subtotal : \(subtotal), \(taxvalue), \(total)")
        
        // tinh toan gia tri cho subtotal
        checkoutview.subtotalvalue.text = "\(subtotal.toShow())"
        checkoutview.taxvalue.text = "\(taxvalue.toShow())"
        checkoutview.totalvalue.text = "\(total.toShow())"
    }
    
    func calcRenewPlusTotal(food : StupidCartObject){
        subtotal += food.stupidprice
        taxvalue += food.stupidprice*0.1
        total = subtotal + taxvalue
        print("subtotal : \(subtotal), \(taxvalue), \(total)")
        
        // tinh toan gia tri cho subtotal
        checkoutview.subtotalvalue.text = "\(subtotal.toShow())"
        checkoutview.taxvalue.text = "\(taxvalue.toShow())"
        checkoutview.totalvalue.text = "\(total.toShow())"
    }
    
    var isloadnewcart = true
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        print("stupid array count : \(stupids.count)")
    }

    func createLeftBar(){
        let menubutton = UIButton(type: UIButtonType.custom)
        menubutton.setTitle(String.materialIcon(font: .ArrowBack), for: .normal)
        menubutton.titleLabel?.font = UIFont.materialIconOfSize(size: 25)
        menubutton.addTarget(self, action: #selector(previousController), for: .touchUpInside)
        
        let leftmenubar = UIBarButtonItem(customView: menubutton)
        
        self.navigationItem.leftBarButtonItem = leftmenubar
    }
    
    @objc func previousController(){
        // dong master controller
        splitViewController?.preferredDisplayMode = UISplitViewControllerDisplayMode.primaryHidden
        
        self.navigationController?.popViewController(animated: true)
    }
    
    var headertitle : UILabel?// day la tieu de cua navigation bar, khi leave khoi controller thi can phai removefromsuperview
    var ringme : UIButton?
    func createRightBar(){
        // chu tren navigation bar
        headertitle = UILabel.init(frame: CGRect(x: self.view.frame.size.width/2-200, y: 0, width: 400, height: 40))
        headertitle?.text = "Menu Order"
        headertitle?.textAlignment = .center
        headertitle?.textColor = UIColor.white
        headertitle?.font = UIFont.boldSystemFont(ofSize: 17)
        self.navigationController?.navigationBar.addSubview(headertitle!)
        // mau cua chu tren navigation bar - la mau trang
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let rightview = UIView(frame: CGRect(x: self.view.frame.size.width - 100, y: 10, width: 100, height: 40))
        
        ringme = UIButton(type: UIButtonType.custom)
        ringme?.frame = CGRect(x: 60, y: 0, width: 40, height: 40)
        ringme?.setTitle(String.materialIcon(font: .AddAlert), for: .normal)
        ringme?.titleLabel?.font = UIFont.materialIconOfSize(size: 25)
        ringme?.addTarget(self, action: #selector(ringMe), for: .touchUpInside)
        
        rightview.addSubview(ringme!)
        
        let rightmenu = UIBarButtonItem(customView: rightview)
        
        self.navigationItem.rightBarButtonItem = rightmenu
    }
    
    @objc func ringMe(){
        ringme?.CCAnimationPop()
    }
    
    @IBOutlet var containercart: UIView!
    @IBOutlet var headercart: HeaderCart!// chua thong tin header thoi
    @IBOutlet var containerorder: UIView!// chua bang tableview cac item duoc order
    var tableview : UITableView? // day la tableview hien thi du lieu chua booking, duoc addsubview boi containerorder
//    var tableviewcart : UITableView? // chua danh sach cart duoc load tu server
    var foods : [FoodObject] = []// danh sach cac food chua duoc booking - mang nay se cong don voi mang carts - la danh sach cac booking cua khac hang duoc load chung tren day
    var carts = CartObject.init()// chua thuoc tinh cartorder la danh sach cac food da duoc booking
    func LoadListOrder(){
        headercart.LoadViewFromNib(nibNameParam: "HeaderCart")
        headercart.layer.backgroundColor = UIColor.gray.cgColor// background cua header
        headercart.CommonStyleView()
        
        containercart.backgroundColor = AppColor.colorgray
        
        tablenobooking()
//        tablehavebooking()
        LoadCheckOutView()
    }
    
    func tablenobooking(){
        tableview = UITableView()
        tableview?.frame = CGRect(x: 0, y: 0, width: containerorder.frame.size.width, height: containerorder.frame.size.height)
        containerorder.addSubview(tableview!)
        containerorder.CommonStyleView()
        
        tableview?.showsHorizontalScrollIndicator = false
        tableview?.showsVerticalScrollIndicator = false
        
        let nib = UINib(nibName: "CartCell", bundle: nil)
        tableview?.register(nib, forCellReuseIdentifier: "ordercell")
        tableview?.dataSource = self
        tableview?.delegate = self
        
        // loai bo cac dong ko co du lieu
        tableview?.tableFooterView = UIView()
        
        tableview?.reloadData()
    }
    
    func tablehavebooking(){
//        tableviewcart = UITableView()
//        tableviewcart?.frame = CGRect(x: 0, y: containerorder.frame.size.height/2, width: containerorder.frame.size.width, height: containerorder.frame.size.height/2)
//        containerorder.addSubview(tableviewcart!)
//        containerorder.CommonStyleView()
        
//        tableviewcart?.showsHorizontalScrollIndicator = false
//        tableviewcart?.showsVerticalScrollIndicator = false
        
//        let nib = UINib(nibName: "CartCell", bundle: nil)
//        tableviewcart?.register(nib, forCellReuseIdentifier: "cartcell")
//        tableviewcart?.dataSource = self
//        tableviewcart?.delegate = self
        
        // loai bo cac dong ko co du lieu
//        tableviewcart?.tableFooterView = UIView()
//
//        tableviewcart?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableView == tableview{
//            return foods.count
//        }
//        else{
//            return carts.cartorder.count
//        }
        return stupids.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CartCell = tableView.dequeueReusableCell(withIdentifier: "ordercell") as! CartCell
        
        // show danh sach cac food chua duoc booking
        if stupids.count > 0{
            //status
            let status = stupids[indexPath.row].stupidstatus
            cell.cartstatus.text = status
            cell.cartstatus.textColor = StatusCollection[status] as! UIColor
//            cell.cartstatus.af_setImage(withURL: URL(string: StatusCollection[status]!)!)
//            cell.cartstatus.iconMenuStyle()
            //name
            print("indexpath : \(indexPath.row)")
            cell.cartname.text = stupids[indexPath.row].stupidname
            // note
            cell.cartnote.text = stupids[indexPath.row].stupidnote// ko co gi vi chua booking, neu khach hang co sua chua thi chi hien tren giao dien -> khi booking thi post theo len
            //quantity
            cell.cartquantity.text = "\(stupids[indexPath.row].stupidquantity)"
            //button sub
            if stupids[indexPath.row].isbooking == true{
                cell.buttonsub.isUserInteractionEnabled = false
                cell.buttonsub.isEnabled = false
                
                cell.buttonplus.isUserInteractionEnabled = false
                cell.buttonplus.isEnabled = false
                
                cell.buttonnote.isUserInteractionEnabled = false
                cell.buttonnote.isEnabled = false
                
                cell.buttondelete.isUserInteractionEnabled = false
                cell.buttondelete.isEnabled = false
            }
            else{
                cell.buttonsub.isUserInteractionEnabled = true
                cell.buttonsub.isEnabled = true
                
                cell.buttonplus.isUserInteractionEnabled = true
                cell.buttonplus.isEnabled = true
                
                cell.buttonnote.isUserInteractionEnabled = true
                cell.buttonnote.isEnabled = true
                
                cell.buttondelete.isUserInteractionEnabled = true
                cell.buttondelete.isEnabled = true
            }
            cell.buttonsub.setCommonButton(name: String.materialIcon(font: .RemoveCircle))
            cell.buttonplus.setCommonButton(name: String.materialIcon(font: .AddCircle))
            
            //price
            cell.cartprice.text = "\(stupids[indexPath.row].stupidprice.toShow())"                // note
            cell.buttonnote.setTitle(String.materialIcon(font: .Edit), for: .normal)
            cell.buttonnote.titleLabel?.font = UIFont.materialIconOfSize(size: 30)
            //delete
            cell.buttondelete.setTitle(String.materialIcon(font: .Delete), for: .normal)
            cell.buttondelete.titleLabel?.font = UIFont.materialIconOfSize(size: 30)
            
            cell.tintColor = AppColor.colorred
            
            //action cua 3 button
            // descrease quantity
            cell.onDescreseQuantity = {
                cell.buttonsub.CCAnimationPop()
                guard let number = Int(cell.cartquantity.text!) else {return}
                var quantity = number
                if quantity > 1{
                    quantity = quantity - 1
                    self.calcRenewSubTotal(food: self.stupids[indexPath.row])
                }
                else{
                    quantity = 1
                }

                cell.cartquantity.text = String(describing: quantity)

                //                self.tableview?.reloadData()
            }
            // increase quantity
            cell.onIncreseQuantity = {
                cell.buttonplus.CCAnimationPop()

                guard let number = Int(cell.cartquantity.text!) else {return}
                var quantity = number
                quantity = quantity + 1
                cell.cartquantity.text = String(describing: quantity)
                //                self.tableview?.reloadData()
                self.calcRenewPlusTotal(food: self.stupids[indexPath.row])
            }
            // edit note
            cell.onEditNote = {
                cell.buttonnote.CCAnimationPop()
                
                let viewnote = self.storyboard?.instantiateViewController(withIdentifier: "viewnote") as! NoteController
                viewnote.noteexist = self.stupids[indexPath.row].stupidnote
                viewnote.indexoforder = indexPath.row
                viewnote.notedelegate = self
                
                self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
                    
                }).didCloseHandler({ (_) in
                    
                })
                
                self.popup?.show(viewnote)
            }

            // delete row
            cell.onDeleteOrder = {
                let questiondelete = UIAlertController(title: "Delete a order", message: "Are you want to delete ?", preferredStyle: UIAlertControllerStyle.alert)

                questiondelete.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.default, handler: { (action) in
                    // remove data
                    self.stupids.remove(at: indexPath.row)
                    //remove row tren table
                    self.tableview?.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)

                    self.tableview?.reloadData()
                    ConfigMessage.show(theme: .success, title: "Completed", body: "Order deleted")
                }))
                questiondelete.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: { (canceled) in
                    self.dismiss(animated: true, completion: nil)
                }))

                self.present(questiondelete, animated: true, completion: nil)
            }
        }
        
        return cell
    }
    
    func RequestNote(notes: [String], index: Int) {
        self.popup?.dismiss()
        
        var updatenote : String = ""
        for note in notes{
            updatenote += "\(note),"
        }
        
        stupids[index].stupidnote = updatenote
        
        tableview?.reloadData()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableview?.cellForRow(at: indexPath) as! CartCell
        print("tableview : \(String(describing: cell.cartquantity.text))")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    //==================== NGUYEN DOAN CHECKOUT ===================//
    @IBOutlet var containercheckout: UIView!
    @IBOutlet var checkoutview: CheckOutView!
    
    var subtotal : Double = 0.0
    var taxvalue : Double = 0.0
    var total    : Double = 0.0
    func LoadCheckOutView(){
        checkoutview.LoadViewFromNib(nibNameParam: "CheckOutView")
        checkoutview.delegate = self
        // tao do bong, bo goc cho ca view
        checkoutview.CommonStyleView()
        // thiet lap style cua cac phan tu o trong
        checkoutview.loadInterface()
        
        containercheckout.backgroundColor = AppColor.colorgray
        
        // tinh toan gia tri cho subtotal
        checkoutview.subtotalvalue.text = "\(subtotal.toShow())"
        checkoutview.taxvalue.text = "\(taxvalue.toShow())"
        checkoutview.totalvalue.text = "\(total.toShow())"
        
        guard let tablename = UserDefaults.standard.string(forKey: "tablename") else {return}
        checkoutview.tableorder.text = tablename
    }
    
    var popup : PopupController?
    
    let cart = CartObject.init()
    func didClickBooking() {
        
        if foods.count > 0{
            viewwarnning = self.storyboard?.instantiateViewController(withIdentifier: "viewwarnning") as! WarnningController
            viewwarnning.warnningdelegate = self
            viewwarnning.type = "bookingcart"
            
            self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
                
            }).didCloseHandler({ (_) in
                
            })
            
            self.popup?.show(viewwarnning)
            
            guard let subtotalString = checkoutview.subtotalvalue.text as NSString? else {return}
            guard let tax = checkoutview.taxvalue.text as NSString? else {return}
            guard let grandtotal = checkoutview.totalvalue.text as NSString? else {return}
            
            // lay thu du lieu so luong
            if tableview != nil{
                var cartorders : [FoodObject] = []
                
                    for row in 0...foods.count-1{
                        let indexpath = IndexPath(row: row, section: 0)
                        guard let cell = tableview?.cellForRow(at: indexpath) as? CartCell else {return}
                        
                        guard let quantity = cell.cartquantity.text as NSString? else {return}
                        guard let note = cell.cartnote.text as NSString? else {return}
                        let footnote : [String] = [note as String]
                        
                        let food = foods[indexpath.row]
                        
                        food.foodquantity = quantity.doubleValue
                        if stupids[row].stupidnote == ""{
                            food.foodnote = footnote
                        }
                        else{
                            let arraystupidnote = stupids[row].stupidnote.components(separatedBy: ",")
                            food.foodnote = arraystupidnote
                        }
                        // truyen them cho server
                        food.foodsubtotal = (quantity.doubleValue)*foods[row].foodprice
                        food.fooddiscount = foods[row].fooddiscount
                        if let vat : Double = UserDefaults.standard.double(forKey: "VAT"){
                            food.foodtotal = (quantity.doubleValue)*foods[row].foodprice + (quantity.doubleValue)*foods[row].foodprice*vat
                        }
                        else{
                            food.foodtotal = (quantity.doubleValue)*foods[row].foodprice
                        }
                        // het truyen them
                        cartorders.append(food)
                    }
                
                cart.cartsubtotal = subtotalString.doubleValue
                cart.carttax = tax.doubleValue
                cart.cartgrandtotal = grandtotal.doubleValue
                cart.cartstatus = "Booking"
                cart.cartorder = cartorders
                // carttable duoc lay khi chon table o popup (phia tren)
                
            }
        }
        else{ // thong bao ko co data de booking
            viewwarnning = self.storyboard?.instantiateViewController(withIdentifier: "viewwarnning") as! WarnningController
            viewwarnning.warnningdelegate = self
            viewwarnning.type = "nodata"
            
            self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
                
            }).didCloseHandler({ (_) in
                
            })
            
            self.popup?.show(viewwarnning)
        }
    }
    
    var viewwarnning : WarnningController!
    func didClickPayment() {
        viewwarnning = self.storyboard?.instantiateViewController(withIdentifier: "viewwarnning") as! WarnningController
        viewwarnning.warnningdelegate = self
        viewwarnning.type = "paymentcart"
        
        self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
            
        }).didCloseHandler({ (_) in
            
        })
        
        self.popup?.show(viewwarnning)
    }
    
    func didClickClear() {
        viewwarnning = self.storyboard?.instantiateViewController(withIdentifier: "viewwarnning") as! WarnningController
        viewwarnning.warnningdelegate = self
        viewwarnning.type = "clearcart"
        
        self.popup = PopupController.create(self).customize([PopupCustomOption.animation(PopupController.PopupAnimation.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler({ (controller) in
            
        }).didCloseHandler({ (_) in
            
        })
        
        self.popup?.show(viewwarnning)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // su kien khi roi khoi controller nay
        isloadnewcart = true
        
        //remove stupid array
        stupids.removeAll()
                
        //reset tong tien khi thoat khoi controller
        subtotal = 0
        taxvalue = 0
        total = 0
        
        ringme?.removeFromSuperview()
        headertitle?.removeFromSuperview()
    }
}
