//
//  SettingCell.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/28/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import GoogleMaterialIconFont

class SettingCell: UICollectionViewCell {
    
    @IBOutlet var foodicon: UIImageView!
    @IBOutlet var foodtitle: UILabel!
    @IBOutlet var foodprice: UILabel!
    @IBOutlet var fooddiscount: UILabel!
    @IBOutlet var btnFoodPlus: UIButton!
    
    func SetupInterface(){
        self.contentView.layer.cornerRadius = 10
        self.contentView.layer.masksToBounds = true
        
        self.foodprice.font = UIFont.boldSystemFont(ofSize: 17)
        
        btnFoodPlus.commonStyleButton()
        btnFoodPlus.titleLabel?.font = UIFont.materialIconOfSize(size: btnFoodPlus.frame.size.width-20)
        btnFoodPlus.setTitle(String.materialIcon(font: MaterialIconFont.Add), for: .normal)
        btnFoodPlus.backgroundColor = AppColor.colorred
        btnFoodPlus.setTitleColor(UIColor.white, for: .normal)
        btnFoodPlus.shadowStyle()
        btnFoodPlus.isSelected = false
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSize(width: 1, height: 1)
        self.layer.shadowOpacity = 1.0//0.2
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.contentView.layer.cornerRadius).cgPath
    }
    
}
