//
//  ConfigIPController.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 9/13/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import PopupController

protocol ConfigIPDelegate : class {
    func SaveConfigIP() // close popup, not any more
}

class ConfigIPController: UIViewController, PopupContentViewController {

    weak var configdelegate : ConfigIPDelegate?
    
    @IBOutlet var txfhttp: SkyFloatingLabelTextField!
    @IBOutlet var txfwebsocket: SkyFloatingLabelTextField!
    @IBOutlet var btnsave: UIButton!
    
    @IBAction func actSave(_ sender: UIButton) {
        // save address to userdefault
        
        if let httpvalue = txfhttp.text{
            UserDefaults.standard.set(httpvalue, forKey: "http")
        }
        if let websocketvalue = txfwebsocket.text{
            UserDefaults.standard.set(websocketvalue, forKey: "websocket")
        }
        
        configdelegate?.SaveConfigIP()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        LoadInterface()
    }
    
    var textFields: [SkyFloatingLabelTextField] = []
    func LoadInterface(){
        
        txfhttp.placeholder = "http address"
        if let http = UserDefaults.standard.string(forKey: "http"){
            txfhttp.text = http
        }
        txfwebsocket.placeholder = "websocket address"
        if let websocket = UserDefaults.standard.string(forKey: "websocket"){
            txfwebsocket.text = websocket
        }
        
        btnsave.setTitle("Save".uppercased(), for: .normal)
        btnsave.backgroundColor = AppColor.colorred
        btnsave.setTitleColor(UIColor.white, for: .normal)// mau cua text
        btnsave.layer.cornerRadius = 20
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 400, height: 450)
    }
}
