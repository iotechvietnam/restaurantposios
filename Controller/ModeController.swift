//
//  ModeController.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/14/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import PopupController

protocol ModeDelegate : class {
    func chooseModeController(table : TableObject)
    func closeModeController()
}

class ModeController: UIViewController, PopupContentViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    weak var modedelegate : ModeDelegate?
    
    @IBOutlet var headermode: UILabel!
    @IBOutlet var ismode: AIFlatSwitch!
    @IBAction func actChangeMode(_ sender: AIFlatSwitch) {
        
        if let isallowtable = UserDefaults.standard.string(forKey: "isallow"){
            if isallowtable == "1"{
                // khi chon mode
                tablecollection.isUserInteractionEnabled = false
                tablecollection.alpha = 0.6
                UserDefaults.standard.set("0", forKey: "isallow")
            }
            else{
                // khong chon mode
                tablecollection.isUserInteractionEnabled = true
                tablecollection.alpha = 1.0
                UserDefaults.standard.set("1", forKey: "isallow")
            }
        }
        
        UserDefaults.standard.synchronize()
    }
    
    
    @IBOutlet var tablecollection: UICollectionView!
    @IBOutlet var btnclose: UIButton!
    @IBAction func actClose(_ sender: UIButton) {
        modedelegate?.closeModeController()
    }
    @IBOutlet var btnok: UIButton!
    @IBAction func actOk(_ sender: UIButton) {

    }
    
    var tables : [TableObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.frame = CGRect(x: 0, y: 0, width: 400, height: 450)
        //interface cua collection
        createCollection()
        LoadData()
        CollectionNotification()
        
        LoadInterface()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func LoadInterface(){
        headermode.text = "Configuration Mode"
        
        if let isallowtable = UserDefaults.standard.string(forKey: "isallow"){
            if isallowtable == "1"{
                ismode.isSelected = true
                tablecollection.isUserInteractionEnabled = true
                tablecollection.alpha = 1.0
            }
            else{
                ismode.isSelected = false
                tablecollection.isUserInteractionEnabled = false
                tablecollection.alpha = 0.6
            }
        }
        
        if btnok != nil{
            btnok.setTitle("Ok".uppercased(), for: .normal)
            btnok.backgroundColor = AppColor.colorred
            btnok.setTitleColor(UIColor.white, for: .normal)// mau cua text
            btnok.layer.cornerRadius = 20
        }
        
        if btnclose != nil{
            btnclose.setTitle("Close".uppercased(), for: .normal)
            btnclose.backgroundColor = UIColor.white
            btnclose.setTitleColor(AppColor.colorred, for: .normal)// mau cua text
            btnclose.layer.cornerRadius = 20
            btnclose.layer.borderWidth = 1
            btnclose.layer.borderColor = AppColor.colorred.cgColor
        }
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 400, height: 450)
    }
    
    func LoadData(){
        TableObject.GetListTable { (tables) in
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: Notification.Name("LoadListTable"), object: tables)
            }
        }
    }
    
    func CollectionNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(LoadListTable(notice:)), name: Notification.Name("LoadListTable"), object: nil)
    }
    
    @objc func LoadListTable(notice : Notification){
        tables = notice.object as! [TableObject]
        
        tablecollection.reloadData()
    }
    
    //=============== COLLECTION ==========
    func createCollection(){
        tablecollection.delegate = self
        tablecollection.dataSource = self
        
        // bo image scroll cua collection
        tablecollection.showsVerticalScrollIndicator = false
        tablecollection.showsHorizontalScrollIndicator = false
        
        // khoang cach giua cac cell
        tablecollection.contentInset = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
        
        // background nen cua ca collection
        tablecollection.backgroundColor = AppColor.colorgray
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tables.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "modecell", for: indexPath as IndexPath) as! ModeCell
        
        cell.tablename.text = tables[indexPath.row].tablename
        cell.setStyleNormal()
        if UserDefaults.standard.string(forKey: "tableid") == tables[indexPath.row].tableid{
            cell.setStyleSelected()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let isallowtable = UserDefaults.standard.string(forKey: "isallow"){
            if isallowtable == "1"{
                // add luon gia tri table vao userdefault
                let userdefault = UserDefaults.standard
                
                userdefault.set(tables[indexPath.row].tableid, forKey: "tableid")
                userdefault.set(tables[indexPath.row].tablename, forKey: "tablename")
                
                modedelegate?.chooseModeController(table: tables[indexPath.row])
            }
        }
    }
    
    // xac dinh so san pham xuat hien tren 1 dong
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSize = (tablecollection.frame.width - (tablecollection.contentInset.left + tablecollection.contentInset.right)-60) / 3
        return CGSize(width: itemSize, height: itemSize*2/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
}
