//
//  ModeCell.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/14/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

class ModeCell: UICollectionViewCell {
    
    @IBOutlet var tablename: UILabel!
    
    
    func setStyleNormal(){
        self.contentView.layer.cornerRadius = 10
        self.contentView.layer.masksToBounds = true
        
        self.tablename.font = UIFont.boldSystemFont(ofSize: 17)
    
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSize(width: 1, height: 1)
        self.layer.shadowOpacity = 1.0//0.2
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowRadius = 2.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.contentView.layer.cornerRadius).cgPath
    }
    
    func setStyleSelected(){
        self.contentView.layer.cornerRadius = 10
        self.contentView.layer.masksToBounds = true
        
        self.tablename.font = UIFont.boldSystemFont(ofSize: 17)
        
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSize(width: 1, height: 1)
        self.layer.shadowOpacity = 1.0//0.2
        
        self.layer.shadowColor = AppColor.colorred.cgColor
        self.layer.shadowRadius = 4.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.contentView.layer.cornerRadius).cgPath
    }
}
