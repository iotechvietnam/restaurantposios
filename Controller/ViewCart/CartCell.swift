//
//  CartCell.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/7/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import SwipyCell

class CartCell: UITableViewCell {
    
    var onDescreseQuantity : (() -> Void)? = nil
    var onIncreseQuantity : (() -> Void)? = nil
    var onEditNote : (() -> Void)? = nil
    var onDeleteOrder : (() -> Void)? = nil

    
    @IBOutlet var cartstatus: UILabel!
    @IBOutlet var cartname: UILabel!
    @IBOutlet var cartnote: UILabel!
    @IBOutlet var buttonsub: UIButton!
    @IBAction func actSub(_ sender: UIButton) {
        if let onDescreseQuantity = self.onDescreseQuantity{
            onDescreseQuantity()
        }
    }
    @IBOutlet var cartquantity: UILabel!
    @IBOutlet var buttonplus: UIButton!
    @IBAction func actPlus(_ sender: UIButton) {
        if let onIncreseQuantity = self.onIncreseQuantity{
            onIncreseQuantity()
        }
    }
    @IBOutlet var cartprice: UILabel!
    @IBOutlet var buttonnote: UIButton!
    @IBAction func actNote(_ sender: UIButton) {
        if let onEditNote = self.onEditNote{
            onEditNote()
        }
    }
    @IBOutlet var buttondelete: UIButton!
    @IBAction func actDelete(_ sender: UIButton) {
        if let onDeleteOrder = self.onDeleteOrder{
            onDeleteOrder()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
