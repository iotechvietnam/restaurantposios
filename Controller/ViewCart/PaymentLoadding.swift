//
//  PaymentLoadding.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/21/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

class PaymentLoadding: UIView {

    @IBOutlet var headerloadding: UILabel!
    
    @IBOutlet var total: UILabel!
    @IBOutlet var subtotal: UILabel!
    @IBOutlet var tax: UILabel!
    
    @IBOutlet var quantitydone: UILabel!
    @IBOutlet var quantitycancel: UILabel!
    @IBOutlet var quantitypendding: UILabel!
    @IBOutlet var quantityaccept: UILabel!
    @IBOutlet var quantitynone: UILabel!
    
    func loadInterface(){
        headerloadding.text = "Table 103"
        
        total.text = "$45.35"
        subtotal.text = "$41.23"
        tax.text = "$4.12"
        
        quantitydone.text = "100"
        quantitycancel.text = "1"
        quantitypendding.text = "0"
        quantityaccept.text = "100"
        quantitynone.text = "0"
    }
}
