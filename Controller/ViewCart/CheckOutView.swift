//
//  CheckOutForm.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/3/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

protocol CheckOutViewDelegate : class {
    func didClickBooking()
    func didClickPayment()
    func didClickClear()
}

class CheckOutView: UIView {
    
    weak var delegate : CheckOutViewDelegate?

    @IBOutlet var tableorder: UILabel!
    @IBOutlet var subtotalvalue: UILabel!
    @IBOutlet var taxvalue: UILabel!
    @IBOutlet var totalvalue: UILabel!
    
    @IBOutlet var btnbooking: UIButton!
    @IBAction func actBooking(_ sender: UIButton) {
        delegate?.didClickBooking()
    }
    
    @IBOutlet var btnpayment: UIButton!
    @IBAction func actPayment(_ sender: UIButton) {
        delegate?.didClickPayment()
    }
    
    @IBOutlet var btnclear: UIButton!
    @IBAction func actClear(_ sender: UIButton) {
        delegate?.didClickClear()
    }
    
    func loadInterface(){
        
        btnbooking.backgroundColor = AppColor.colorred
        btnbooking.setImage(UIImage(named: "ic_home_hi.png"), for: .normal)
        btnbooking.setTitle("Booking".uppercased(), for: .normal)
        btnbooking.setTitleColor(UIColor.white, for: .normal)// mau cua text
        btnbooking.tintColor = UIColor.white// mau cua image
        
        btnbooking.checkoutStyle()
        
        btnpayment.backgroundColor = UIColor.white
        btnpayment.setImage(UIImage(named: "payment.png"), for: .normal)
        btnpayment.setTitle("Payment".uppercased(), for: .normal)
        btnpayment.setTitleColor(AppColor.colorred, for: .normal)
        btnpayment.tintColor = AppColor.colorred
        
        btnpayment.checkoutStyle()
        
        btnclear.backgroundColor = UIColor.white
        btnclear.setImage(UIImage(named: "clear.png"), for: .normal)
        btnclear.setTitle("Clean All".uppercased(), for: .normal)
        btnclear.setTitleColor(UIColor.gray, for: .normal)
        btnclear.tintColor = UIColor.gray
        
        btnclear.checkoutStyle()
        
        // set tam data cho khach hang xem
//        subtotalvalue.text = "$41.23"
//        taxvalue.text = "$4.12"
//        totalvalue.text = "$45.35"
    }
    
}
