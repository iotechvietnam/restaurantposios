//
//  TablesController.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/14/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import PopupController
import GoogleMaterialIconFont

protocol TablesDelegate : class {
    func closeTablesController()
}

class TablesController: UIViewController, PopupContentViewController {

    weak var tabledelegate : TablesDelegate?
    
    @IBOutlet var btnclose: UIButton!
    @IBOutlet var headertable: UILabel!
    @IBAction func actClose(_ sender: UIButton) {
        tabledelegate?.closeTablesController()
    }
    @IBOutlet var tableselection: UIButton!
    @IBAction func actSelection(_ sender: UIButton) {
        tabledropdown.show()
    }
    @IBOutlet var tabledropdown: DropDown!
    
    @IBOutlet var viewcontent: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.frame = CGRect(x: 0, y: 0, width: 400, height: 450)
        
        LoadInterface()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 400, height: 450)
    }

    func LoadInterface(){
        headertable.text = "Tables"
        
        if btnclose != nil{
            btnclose.setCommonButton(name: String.materialIcon(font: .Close))
        }
        
        tabledropdown.anchorView = view
        
        tabledropdown.direction = .bottom
        
        tabledropdown.dataSource = ["Table 101","Table 102","Table 103","Table 104","Table 105"]
        
        tabledropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tableselection.setTitle(item, for: .normal)
        }
        
        tableselection.setTitle("Table 101", for: .normal)
        tableselection.setTitleColor(AppColor.colortext, for: .normal)
        tableselection.layer.borderWidth = 1
        tableselection.layer.borderColor = AppColor.colorgray.cgColor
        // padding text ra 1 doan
        tableselection.titleEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        viewcontent.layer.borderWidth = 1
        viewcontent.layer.borderColor = AppColor.colorgray.cgColor
    }
}
