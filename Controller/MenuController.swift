//
//  MasterController.swift
//  SplitViewController
//
//  Created by KingCQ on 16/5/19.
//  Copyright © 2016年 KingCQ. All rights reserved.
//

import UIKit
import AlamofireImage
import GoogleMaterialIconFont

import RxStarscream
import Starscream

class MenuController: UITableViewController,UISplitViewControllerDelegate {
    
    var firstCome: Bool = true
    
    var collapseDetailViewController: Bool  = true
    
    var categories : [CategoryObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetupTableView()

        splitViewController?.delegate = self
        splitViewController?.view.backgroundColor = UIColor.clear//AppColor.colorblack
        
        // mau cua top bar
//        self.navigationController?.navigationBar.barTintColor = AppColor.colorred
        // bo han navigation bar
        self.navigationController?.navigationBar.isHidden = true

        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        LoadData()
        CollectionNotification()
    }
    
    func SetupTableView(){
        splitViewController?.view.isOpaque = false
        splitViewController?.view.backgroundColor = UIColor.clear
        
//        tableView.layer.borderWidth = 1
        tableView.layer.borderColor = UIColor.clear.cgColor
        print("\(tableView.frame.size.height)")
        
        tableView.separatorStyle = .none
        
        // day la set background cua toan bo tableview
        tableView.backgroundColor = UIColor.white
        
        let nibheader = UINib(nibName: "LeftMenuHeaderCell", bundle: nil)
        tableView?.register(nibheader, forCellReuseIdentifier: "headercell")
        let nib = UINib(nibName: "ImageCell", bundle: nil)
        tableView?.register(nib, forCellReuseIdentifier: "menucell")
        
        // loai bo cac dong ko co du lieu
        tableView.tableFooterView = UIView()
        
        tableView.reloadData()
    }
    
    func CollectionNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(LoadAllCategories(notice:)), name: Notification.Name("LoadAllCategories"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InsertedCategory(notice:)), name: Notification.Name("InsertedCategory"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UpdatedCategory(notice:)), name: Notification.Name("UpdatedCategory"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DeletedCategory(notice:)), name: Notification.Name("DeletedCategory"), object: nil)
    }
    
    @objc func LoadAllCategories(notice : Notification){
        let arraydata = notice.object as! [CategoryObject]
        
        categories = arraydata
        
        if categories.count > 0{
            if UIScreen.main.bounds.height < UIScreen.main.bounds.width {
                if firstCome {
                    firstCome = false
                    performSegue(withIdentifier: "go_to_setting", sender: categories[0])
                }
            }
        }
        // phai lam moi data
        tableView.reloadData()
    }
    
    @objc func InsertedCategory(notice : Notification){
        let cateobj = notice.object as! CategoryObject
        
        categories.append(cateobj)
        
        // phai lam moi data
        tableView.reloadData()
    }
    
    @objc func UpdatedCategory(notice : Notification){
        let cateobj = notice.object as! CategoryObject
        
        // duyet mang de xem phan tu nao thay doi
        categories.filter({$0.cateid == cateobj.cateid}).forEach {
            if cateobj.catename != ""{
                $0.catename = cateobj.catename
            }
            if cateobj.cateicon != ""{
                $0.cateicon = cateobj.cateicon
            }
            if cateobj.cateorderby != 3900000{ // 3900000 la gia tri mac dinh
                $0.cateorderby = cateobj.cateorderby
            }
        }
        
        // phai lam moi data
        tableView.reloadData()
    }
    
    @objc func DeletedCategory(notice : Notification){
        let cateid = notice.object as! String
        
        // lay ve index cua object can xoa dua vao cateid
        let index = categories.index(where : { $0.cateid == cateid })
        // xoa doi tuong trong mang co cateid
        categories.remove(at: index!)
//        categories.filter { $0.cateid != cateid }
        
        // phai lam moi data
        tableView.reloadData()
    }
    
    @objc func showListCart(){
    
    }
    
    func LoadData(){
        let socket = WebSocket(url: URL(string: "\(websocket)/categories")!)
        socket.connect()
        
        socket.rx.connected.subscribe(onNext: { (isconnected) in
            socket.rx.text.subscribe(onNext: { (jsondata) in

                CategoryObject.IsOperationRealtimeData(responseString: jsondata, callback: { (realtime) in
                    if realtime == "get"{ // load lan dau tien => lay ve toan bo categories
                        CategoryObject.GetAllCate(responseString: jsondata, callback: { (responsearray) in
                            DispatchQueue.main.async {
                                NotificationCenter.default.post(name: Notification.Name("LoadAllCategories"), object: responsearray)
                            }
                        })
                    }
                    else{ // xay ra truong hop inserted, updated, deleted
                        if realtime == "insert"{// insert
                            CategoryObject.InsertRealtimeData(responseString: jsondata, inserted: { (category) in
                                DispatchQueue.main.async {
                                    NotificationCenter.default.post(name: Notification.Name("InsertedCategory"), object: category)
                                }
                            })
                        }
                        else if realtime == "update"{ // update
                            CategoryObject.UpdateRealtimeData(responseString: jsondata, updated: { (category) in
                                DispatchQueue.main.async {
                                    NotificationCenter.default.post(name: Notification.Name("UpdatedCategory"), object: category)
                                }
                            })
                        }
                        else if realtime == "delete"{ // delete
                            CategoryObject.DeleteRealtimeData(responseString: jsondata, deleted: { (cateid) in
                                DispatchQueue.main.async {
                                    NotificationCenter.default.post(name: Notification.Name("DeletedCategory"), object: cateid)
                                }
                            })
                        }
                    }
                })
                
            }, onError: { (err) in
                print("Error!!!")
            }, onCompleted: {
                print("Completed")
            }) {
                print("Disposed")
            }
        }, onError: { (error) in
            print("Error")
        }, onCompleted: {
            print("completed")
        }) {
            print("Disposed")
        }
    }
    
    //MARK: - tableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count+2// phan tu 0 : la textfield, phan tu 1 la : popular
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell: ImageCell = tableView.dequeueReusableCell(withIdentifier: "menucell") as! ImageCell
        
//        if categories.count > 0{
        
            if indexPath.row == 0{
                
                let cell: LeftMenuHeaderCell = tableView.dequeueReusableCell(withIdentifier: "headercell") as! LeftMenuHeaderCell
                
                if let iconlogo = UserDefaults.standard.string(forKey: "iconlogo"){
                    if let urlhttp = UserDefaults.standard.string(forKey: "http"){
                        cell.iconlogo.af_setImage(withURL: URL(string: "\(urlhttp)/icon?image=\(iconlogo)")!)
                        cell.iconlogo.iconMenuStyle()
                    }
                }
                if let namelogo = UserDefaults.standard.string(forKey: "namelogo"){
                    cell.companyname.text = namelogo
                }
                
                cell.backgroundColor = AppColor.colorred
                
                cell.selectionStyle = .none
                
                return cell
            }
            else if indexPath.row == 1{
                let cell: ImageCell = tableView.dequeueReusableCell(withIdentifier: "menucell") as! ImageCell
                
                cell.iconmenu.image = UIImage(named: "ic_home.png")
                cell.iconmenu.commonStyleImageView()
                
                cell.catname.text = "Outbox"
                
                cell.backgroundColor = UIColor.white//AppColor.colorred
                
                cell.iconmenu.iconMenuStyle()
                
                cell.selectionStyle = .none
                
                return cell
            }
            else{
                let cell: ImageCell = tableView.dequeueReusableCell(withIdentifier: "menucell") as! ImageCell
                
                if categories.count > 0{
                    cell.iconmenu.af_setImage(withURL: URL(string: categories[indexPath.row-2].cateicon)!)// vi la dong dau tien co dinh nen mang phai - 2
                    
                    cell.catname.text = categories[indexPath.row-2].catename
                    
                    cell.backgroundColor = UIColor.white
                    
                    cell.iconmenu.iconMenuStyle()
                }
                
                cell.selectionStyle = .none
                
                return cell
            }
            
            //set background cho phan data co so lieu
//            cell.backgroundColor = AppColor.colorblack
//        }
        
        // set separator full cell
//        cell.preservesSuperviewLayoutMargins = false
//        cell.separatorInset = UIEdgeInsets.zero
//        cell.layoutMargins = UIEdgeInsets.zero
        
//        return cell
    }
    //MARK: - tableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        collapseDetailViewController = !collapseDetailViewController
        
        splitViewController?.preferredDisplayMode = UISplitViewControllerDisplayMode.primaryHidden
        
        if indexPath.row == 0{
            
        }
        else if indexPath.row == 1{
            performSegue(withIdentifier: "go_to_setting", sender: 0)
        }
        else{
            performSegue(withIdentifier: "to_list_item", sender: categories[indexPath.row-2])
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "to_list_item" {
            if let destNavigationController: UINavigationController = segue.destination as? UINavigationController {
                let controller = destNavigationController.topViewController as! ListItemController
                
                let cateobj = sender as? CategoryObject
                controller.cateid = cateobj?.cateid
                controller.title = cateobj?.catename
            }
        }
        if segue.identifier == "go_to_setting"{
            if let destNavigationController: UINavigationController = segue.destination as? UINavigationController {
                let controller = destNavigationController.topViewController as! SettingController
                
                controller.title = "Outbox"
        
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 150
        }
        else{
            return 64
        }
    }
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: IndexPath) {
        if let cell:ImageCell = tableView.cellForRow(at: indexPath) as? ImageCell {
            cell.iconmenu.image = UIImage(named: "ic_home_hi.png")
        }
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: IndexPath) {
        if let cell:ImageCell = tableView.cellForRow(at: indexPath) as? ImageCell{
            cell.iconmenu.image = UIImage(named: "ic_home.png")
        }
    }
    
    //MARK: - UISplitViewControllerDelegate
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {
        return collapseDetailViewController
    }
    
    @objc func hel() {
        
    }
    
}
