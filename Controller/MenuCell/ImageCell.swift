//
//  ImageCell.swift
//  Restaurant4POS
//
//  Created by ltk1205 on 7/30/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {

    @IBOutlet var catname: UILabel!
    @IBOutlet var iconmenu: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
