//
//  LeftMenuHeaderCell.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/17/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

class LeftMenuHeaderCell: UITableViewCell {

    @IBOutlet var iconlogo: UIImageView!
    @IBOutlet var companyname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
