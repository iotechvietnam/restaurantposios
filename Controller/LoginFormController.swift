//
//  LoginFormController.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 9/13/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import PopupController

protocol LoginDelegate : class {
    func ActionToLogin()
}

class LoginFormController: UIViewController, PopupContentViewController {

    weak var logindelegate : LoginDelegate?
    
    @IBOutlet var username: SkyFloatingLabelTextField!
    @IBOutlet var password: SkyFloatingLabelTextField!
    @IBOutlet var btnsave: UIButton!
    
    @IBAction func ActLogin(_ sender: UIButton) {
        guard let txtusername = username.text else {return}
        guard let txtpassword = password.text else {return}
        // call func login (API) from server
        SettingObject.LoginAccount(username: txtusername, password: txtpassword) { (mess) in
            if mess == true{
                ConfigMessage.show(theme: .success, title: "Login/Logout", body: "Login successful")
                self.logindelegate?.ActionToLogin()
                UserDefaults.standard.set("1", forKey: "islogin")
            }
            else{
                ConfigMessage.show(theme: .error, title: "Action Message", body: "Login fail")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        LoadInterface()
    }
    
    var textFields: [SkyFloatingLabelTextField] = []
    func LoadInterface(){
        
        username.placeholder = "Username"
        password.placeholder = "Password"
        
        btnsave.setTitle("Login".uppercased(), for: .normal)
        btnsave.backgroundColor = AppColor.colorred
        btnsave.setTitleColor(UIColor.white, for: .normal)// mau cua text
        btnsave.layer.cornerRadius = 20
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 400, height: 450)
    }
}
