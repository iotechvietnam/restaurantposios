//
//  WarnningController.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/20/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import PopupController
import GoogleMaterialIconFont

protocol WarnningDelegate : class {
    func closeWarnningController()
    func acceptWarnningController()
}

class WarnningController: UIViewController, PopupContentViewController {

    weak var warnningdelegate : WarnningDelegate?
    
    var type : String = "warnning"
    
    @IBOutlet var headerwarnning: UILabel!
    @IBOutlet var btnclose: UIButton!
    @IBAction func actClose(_ sender: UIButton) {
        warnningdelegate?.closeWarnningController()
    }
    @IBOutlet var contentwarnning: UILabel!
    @IBOutlet var btnaccept: UIButton!
    @IBAction func actAccept(_ sender: UIButton) {
        warnningdelegate?.acceptWarnningController()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.frame = CGRect(x: 0, y: 0, width:400, height: 250)
        switch type {
        case "warnning":
            self.LoadInterface(header: "Logout Process", content: "Are you sure logout now Are you sure logout now?")
            break
        case "clearcart":
            self.LoadInterface(header: "Remove Order", content: "Are you sure want to remove this order?")
            break
        case "paymentcart":
            self.LoadInterface(header: "Payment a Order", content: "Are you sure pay your order?")
            break
        case "bookingcart":
            self.LoadInterface(header: "Booking a Order", content: "Are you sure booking this order?")
            break
        case "paymentsuccess":
            self.LoadInterface(header: "Thank for Purchased", content: "Your payment is completed")
            break
        case "nodata":
            self.LoadInterface(header: "No data avaiable", content: "Choose least a order before booking")
            break
        case "payementfailure":
            self.LoadInterface(header: "Can't payment at this time", content: "There are errors occur")
            break
        case "popuptable":
            self.LoadInterface(header: "Table not is set", content: "You need choose a table")
            break
        default:
            self.LoadInterface(header: "Logout Process", content: "Are you sure logout ?")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 400, height: 250)
    }
    
    func LoadInterface(header : String, content : String){
        if headerwarnning != nil{
            headerwarnning.text = header
        }
        if contentwarnning != nil{
            contentwarnning.text = content
            // cho xuong dong neu nhieu chu qua
            contentwarnning.numberOfLines = 0
            contentwarnning.lineBreakMode = .byWordWrapping
        }
        
        if btnclose != nil{
//            btnclose.setCommonButton(name: String.materialIcon(font: .Close))
        }
        
        if btnaccept != nil{
            btnaccept.setTitle("Ok", for: .normal)
            btnaccept.addCartStyle()
            btnaccept.layer.cornerRadius = 25
        }
    }
}
