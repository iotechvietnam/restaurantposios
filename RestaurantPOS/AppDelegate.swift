//
//  AppDelegate.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/2/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if let splitController = self.window!.rootViewController as? UISplitViewController {
            let navigationController = splitController.viewControllers[splitController.viewControllers.count-1] as! UINavigationController
            
                splitController.preferredDisplayMode = UISplitViewControllerDisplayMode.primaryHidden
            
            // loai bo dong ke giua 2 controller
            splitController.view.isOpaque = false
            let imgdevider = UIView.init(frame: CGRect(x: 250, y: 0, width: 1, height: (window?.frame.size.height)!))
            imgdevider.backgroundColor = UIColor.clear
            splitController.view.addSubview(imgdevider)
            splitController.view.backgroundColor = UIColor.clear
            
//            let windows: [UIWindow] = UIApplication.shared.windows
//            let firstWindow: UIWindow = windows[0]
//
//            let loadingView = UIView.init(frame: CGRect(x: 250, y: 0, width: 1, height: (window?.frame.size.height)!))
//            loadingView.backgroundColor = UIColor.red
//
//            firstWindow.addSubview(loadingView)
//            loadingView.bringSubview(toFront: firstWindow)
            navigationController.topViewController!.navigationItem.leftItemsSupplementBackButton = true
            
            //them progess view vao man hinh screen
//            let data = ActivityData()
//            NVActivityIndicatorPresenter.sharedInstance.startAnimating(data)
//
//            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
//                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//            }
        }
        
        GetSettingParameter()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func GetSettingParameter(){
        
        let userdefault = UserDefaults.standard
        
        // set default for http and websocket
        if userdefault.string(forKey: "http") == nil{
            userdefault.set(host, forKey: "http")
        }
        if userdefault.string(forKey: "websocket") == nil{
            userdefault.set(websocket, forKey: "websocket")
        }
        
        if userdefault.string(forKey: "islogin") == nil{
            userdefault.set("0", forKey: "islogin")
        }
        // 0 - account not login, 1 - account logged
        
        // selected table - allow choose table or not
        if userdefault.string(forKey: "isallow") == nil{
            userdefault.set("1", forKey: "isallow")
        }
        
        SettingObject.GetStaticParameter { (currency, listcurrency, language, listlanguage, VAT, icon, name) in
            
            // currency
            if userdefault.string(forKey: "currency") == nil{
                userdefault.set(currency, forKey: "currency")
            }
            // list currency
            if userdefault.string(forKey: "listcurrency") == nil{
                userdefault.set(listcurrency, forKey: "listcurrency")
            }
            // language
            if userdefault.string(forKey: "language") == nil{
                userdefault.set(language, forKey: "language")
            }
            // list language
            if userdefault.string(forKey: "listlanguage") == nil{
                userdefault.set(listlanguage, forKey: "listlanguage")
            }
            
            // VAT
            if userdefault.string(forKey: "VAT") == nil{
                userdefault.set(VAT, forKey: "VAT")
            }
            
            // LOGO
            if userdefault.string(forKey: "iconlogo") == nil{
                userdefault.set(icon, forKey: "iconlogo")
            }
            if userdefault.string(forKey: "namelogo") == nil{
                userdefault.set(name, forKey: "namelogo")
            }
        }
    }
    
}

