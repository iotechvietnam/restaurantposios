//
//  NoteObject.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 9/7/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NoteObject: NSObject {

    var noteid  : String = ""
    var notename: String = ""
    
    override init() {
        super.init()
    }
    
    init?(json : [String:Any]){
        if let noteid = json["_id"] as? String{
            self.noteid = noteid
        }
        if let notename = json["name"] as? String{
            self.notename = notename
        }
    }
    
    static func GetListNotes(callback : @escaping ([NoteObject]) -> ()){
        
        let header = [String : String]()
        
        var stringurl : String = ""
        if let urlhost = UserDefaults.standard.string(forKey: "http"){
            stringurl = "\(urlhost)/notes"
        }
        
        let url = URL.init(string: stringurl)
        
        Alamofire.request(url!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{ (responseData) -> Void in
            
            var notes = [NoteObject]()
            if (responseData.result.value != nil){
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                for case let result in swiftyJsonVar.arrayValue{
                    let dict = result.dictionaryObject
                    let note = NoteObject.init(json: dict!)
                    notes.append(note!)
                }
                callback(notes)
            }
        }
    }
}
