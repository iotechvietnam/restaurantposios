//
//  StypidCartObject.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 9/7/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit

class StupidCartObject: NSObject {

    var stupidname      : String = ""
    var stupidquantity  : Int = 0
    var stupidprice     : Double = 0.0
    var stupidstatus    : String = ""
    var stupidnote      : String = ""
    var isbooking       : Bool = false
    
    override init() {
        super.init()
    }
}
