//
//  CategoryObject.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 7/24/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryObject: NSObject {

    var cateid          : String = ""
    var catename        : String = ""
    var cateicon        : String = "https://images-platform.99static.com/m3DT_DYAvaeWLqD4RAva0fde_FU=/fit-in/900x675/99designs-contests-attachments/34/34174/attachment_34174793"
    var cateorderby     : Int    = 3900000
    var catecreatedate  : String = ""
    
    override init() {
        super.init()
    }
    
//    init(name: String, imageurl : String){
//        self.catename = name
//        self.cateicon = imageurl
//    }
    
    init?(json : [String:Any]){
        if let cateid = json["_id"] as? String{
            self.cateid = cateid
        }
        if let catename = json["name"] as? String{
            self.catename = catename
        }
        if let cateicon = json["icon"] as? String{
            self.cateicon = cateicon
        }
        if let cateorderby = json["orderby"] as? Int{
            self.cateorderby = cateorderby
        }
        if let catecreatedate = json["createdDate"] as? String{
            self.catecreatedate = catecreatedate
        }
    }
    
    // lay tat ca danh muc categories
    static func GetAllCate(responseString : String ,callback : @escaping ([CategoryObject])->()){
        let responseData = JSON.init(parseJSON: responseString)

        var categories : [CategoryObject] = []
        
        let result = responseData.dictionaryValue["fullDocument"]

        for dict in (result?.arrayValue)!{
            let category = CategoryObject.init(json: dict.dictionaryObject!)
            categories.append(category!)
        }
        callback(categories)
    }
    
    static func IsOperationRealtimeData(responseString : String ,callback : @escaping (String)->()){
        let responseData = JSON.init(parseJSON: responseString)
        
        let operationtype = responseData["operationType"].stringValue
        
        callback(operationtype)
    }
    
    // quan sat va cap nhat data thoi gian thuc khi thay doi categories (trong truong hop insert)
    static func InsertRealtimeData(responseString : String ,inserted : @escaping (CategoryObject)->()){
        let responseData = JSON.init(parseJSON: responseString)

        // lay toan bo data can insert la fullDocument
        let dict = responseData["fullDocument"].dictionaryObject
        let category = CategoryObject.init(json: dict!)
        
        inserted(category!)
    }
    
    // quan sat va cap nhat data thoi gian thuc khi thay doi categories (trong truong hop updated)
    static func UpdateRealtimeData(responseString : String ,updated : @escaping (CategoryObject)->()){
        let responseData = JSON.init(parseJSON: responseString)
        
        // update co 2 loai - updatefields 
        let dict = responseData["fullDocument"].dictionaryObject
        let category = CategoryObject.init(json: dict!)
        // lay ve cateid (cateid lay sau vi data o 2 nhanh)
        let cateid = responseData["documentKey"].stringValue
        category?.cateid = cateid
        
        updated(category!)
    }
    
    static func DeleteRealtimeData(responseString : String ,deleted : @escaping (String)->()){
        let responseData = JSON.init(parseJSON: responseString)
        
        let cateid = responseData["documentKey"].stringValue
        
        deleted(cateid)
    }
}
