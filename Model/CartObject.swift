//
//  CartObject.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/7/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class CartObject: NSObject {

    var cartid          : String = ""
    var carttable       = TableObject.init()
    var cartstatus      : String = ""
    var cartsubtotal    : Double = 0.0
    var cartdiscount    : Double = 0.0
    var carttax         : Double = 0.0
    var cartgrandtotal  : Double = 0.0
//    var cartcurrency    : String = ""
    var cartorder       : [FoodObject] = []
    
    override init() {
        super.init()
    }
    
    init?(jsoninput : [String:Any]){
        if let json = jsoninput["fullDocument"] as? [String : Any]{
            
            if let cartid = json["_id"] as? String{
                self.cartid = cartid
            }
            if let cartsubtotal = json["subTotal"] as? Double{
                self.cartsubtotal = cartsubtotal
            }
            if let cartdiscount = json["discount"] as? Double{
                self.cartdiscount = cartdiscount
            }
            if let cartgrandtotal = json["grandTotal"] as? Double{
                self.cartgrandtotal = cartgrandtotal
            }
            if let carttax = json["VAT"] as? Double{
                self.carttax = carttax
            }
            if let cartstatus = json["status"] as? String{
                self.cartstatus = cartstatus
            }
            
            //table
            let table = TableObject.init()
            if let jsontable = json["table"] as? [String : Any] {
                if let tableid = jsontable["_id"] as? String{
                    table.tableid = tableid
                }
                if let tablename = jsontable["name"] as? String{
                    table.tablename = tablename
                }
            }
            // danh sach order
            if let jsonorder = json["order"] as? [[String : Any]]{
                var orders = [FoodObject]()
                for dict in jsonorder{
                    let order = FoodObject.init()// order object thuc ra la food object
                    
                    if let orderid = dict["_id"] as? String{
                        order.orderid = orderid
                    }
                    if let quantity = dict["quantity"] as? Double{
                        order.foodquantity = quantity
                    }
                    if let subtotal = dict["subTotal"] as? Double{
                        order.foodsubtotal = subtotal
                    }
                    if let discount = dict["discount"] as? Double{
                        order.fooddiscount = discount
                    }
                    if let status = dict["status"] as? String{
                        order.foodstatus = status
                    }
                    if let priority = dict["priority"] as? Bool{
                        order.foodpriority = priority
                    }
                    if let jsonfood = dict["food"] as? [String : Any]{
                        
                        if let foodid = jsonfood["_id"] as? String{
                            order.foodid = foodid
                        }
                        if let foodname = jsonfood["name"] as? String{
                            order.foodname = foodname
                        }
                        if let foodprice = jsonfood["price"] as? Double{
                            order.foodprice = foodprice
                        }
                        //                        if let fooddiscount = jsonfood["saleOff"] as? Float{
                        //                            order.fooddiscount = fooddiscount
                        //                        }// thua vi trung voi cai discount o tren
                        if let foodcurrency = jsonfood["initPrice"] as? String{
                            order.foodcurrency = foodcurrency
                        }
                    }
                    if let ordernote = dict["note"] as? [String]{
                        order.foodnote = ordernote
                    }
                    
                    orders.append(order)
                }
                self.cartorder = orders
            }
        }
    }
    
    static func IsRealtimeInSelfTable(responseString : String ,callback : @escaping (String)->()){
        let responseData = JSON.init(parseJSON: responseString)
        print(responseData)
        let tableid = responseData["fullDocument"]["table"]["_id"].stringValue
    
        callback(tableid)
    }
    
    // lay tat ca danh muc Cart
    static func GetAllCart(responseString : String ,success : @escaping (CartObject)->(), failure : @escaping (String)->()){
        if responseString.count > 2{
            let responseData = JSON.init(parseJSON: responseString)
            
            guard let cart = CartObject.init(jsoninput: responseData.dictionaryObject!) else {return}
            
            success(cart)
        }
        else{
            failure("failure")
        }
    }
    
    // payment
    static func PaymentABooking(cart : CartObject, callback : @escaping (String) -> ()){
        
//        var header = [String : String]()
//        header.updateValue("application/json", forKey: "Content-Type")
        
        var stringurl : String = ""
        if let urlhost = UserDefaults.standard.string(forKey: "http"){
            stringurl = "\(urlhost)/cart/update/status/\(cart.cartid)"
        }
        
        let url = URL.init(string: stringurl)
        
        let parameter : [String : Any] = [
            "status"           : "Checkout",
        ]
        
        var request = URLRequest(url: url!)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameter)
        
        Alamofire.request(request).responseJSON { (responseData) in
            if responseData.result.value != nil{
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                let status =  swiftyJsonVar["status"].stringValue
                callback(status)
            }
        }
        
//        Alamofire.request(url!, method: .put, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON { (responseData) in
//            if responseData.result.value != nil{
//                let swiftyJsonVar = JSON(responseData.result.value!)
//
//                let status =  swiftyJsonVar["status"].stringValue
//                callback(status)
//            }
//        }
    }
    
    // clear order
    static func ClearABooking(cart : CartObject, callback : @escaping (String) -> ()){
        var header = [String : String]()
        header.updateValue("application/json", forKey: "Content-Type")
        
        var stringurl : String = ""
        if let urlhost = UserDefaults.standard.string(forKey: "http"){
            stringurl = "\(urlhost)/cart/order/delete/id=\(cart.cartid)"
        }
        
        let url = URL.init(string: stringurl)
        
        let parameter : [String : Any] = [:]
            
        Alamofire.request(url!, method: .delete, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON { (responseData) in
            if responseData.result.value != nil{
                let swiftyJsonVar = JSON(responseData.result.value!)

                let status =  swiftyJsonVar["status"].stringValue
                callback(status)
            }
        }
    }
}
