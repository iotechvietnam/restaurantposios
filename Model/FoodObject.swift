//
//  FoodObject.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/27/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FoodObject: NSObject, NSCoding {
    func encode(with aCoder: NSCoder) {
        aCoder.encode(foodid, forKey: "foodid")
        aCoder.encode(foodname, forKey: "foodname")
        aCoder.encode(foodprice, forKey: "foodprice")
        aCoder.encode(foodcurrency, forKey: "foodcurrency")
        aCoder.encode(fooddescription, forKey: "fooddescription")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.foodid = aDecoder.decodeObject(forKey: "foodid") as! String
        self.foodname = aDecoder.decodeObject(forKey: "foodname") as! String
        if let price = aDecoder.decodeObject(forKey: "foodprice") as? Double{
           self.foodprice = price
        }
        self.foodcurrency = aDecoder.decodeObject(forKey: "foodcurrency") as! String
        self.fooddescription = aDecoder.decodeObject(forKey: "fooddescription") as! String
    }
    

    var foodid          : String = ""
    var foodcategory    : [CategoryObject] = []
    var foodname        : String = ""
    var foodimages      : [String] = []
    var foodprice       : Double = 0.0
    var foodcurrency    : String = ""
    var fooddescription : String = ""
    var fooddiscount    : Double = 0.0
    var foodinstock     : Bool = false
    var foodishome      : Bool = false
    var foodcreatedate  : String = ""
    
    // du lieu nay chi de luu them, ko xu ly
    var orderid         : String = ""
    var foodquantity    : Double = 1.0
    var foodsubtotal    : Double = 0.0
    var foodtotal       : Double = 0.0
    var foodstatus      : String = ""
    var foodpriority    : Bool = false
    var foodnote        : [String] = []
    
    override init() {
        super.init()
    }
    
    init?(json : [String:Any]){
        if let foodid = json["_id"] as? String{
            self.foodid = foodid
        }
        if let foodname = json["name"] as? String{
            self.foodname = foodname
        }
        if let foodprice = json["price"] as? Double{
            self.foodprice = foodprice
        }
        if let foodcurrency = json["unitPrice"] as? String{
            self.foodcurrency = foodcurrency
        }
        if let fooddescription = json["description"] as? String{
            self.fooddescription = fooddescription
        }
        if let fooddiscount = json["saleOff"] as? Double{
            self.fooddiscount = fooddiscount
        }
        if let foodinstock = json["inStock"] as? Bool{
            self.foodinstock = foodinstock
        }
        if let foodishome = json["isHome"] as? Bool{
            self.foodishome = foodishome
        }
        if let foodcreatedate = json["createDate"] as? String{
            self.foodcreatedate = foodcreatedate
        }
        
        // images - la 1 mang cac string
        if let dictimage = json["images"] as? [String]{
            var images = [String]()
            for image in dictimage{
                images.append(image)
            }
            
            self.foodimages = images
        }
        
        // foodcategory la 1 mang cac doi tuong
        if let foodcategories = json["category"] as? [[String:Any]]{
            var categories = [CategoryObject]()
            
            for dict in foodcategories{
                let category = CategoryObject()
                
                if let cateid = dict["_id"] as? String{
                    category.cateid = cateid
                }
                if let catename = dict["name"] as? String{
                    category.catename = catename
                }
                categories.append(category)
            }
            
            self.foodcategory = categories
        }
    }
    
    init?(jsonrealtime : [String:Any]){
        
    }
    
    static func GetFoodDataStatic(responseString : String, callback : @escaping ([FoodObject]) -> ()){
        let responseData = JSON.init(parseJSON: responseString)
        
        var foods : [FoodObject] = []
        
        let result = responseData.dictionaryValue["fullDocument"]
        
        for dict in (result?.arrayValue)!{
            let food = FoodObject.init(json: dict.dictionaryObject!)
            foods.append(food!)
        }
        callback(foods)
    }
    
    static func GetFoodDataWithID(cateid : String, callback : @escaping ([FoodObject]) -> ()){
        
        let header = [String : String]()
        
        var stringurl : String = ""
        if let urlhost = UserDefaults.standard.string(forKey: "http"){
            stringurl = "\(urlhost)/foods/category/\(cateid)"
        }
        
        let url = URL.init(string: stringurl)
        
        Alamofire.request(url!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{ (responseData) -> Void in
            
            var foods = [FoodObject]()
            if (responseData.result.value != nil){
                let swiftyJsonVar = JSON(responseData.result.value!)

                for case let result in swiftyJsonVar.arrayValue{
                    let dict = result.dictionaryObject
                    let food = FoodObject.init(json: dict!)
                    foods.append(food!)
                }
                callback(foods)
            }
        }
    }
    
    static func BookingOrder(cart : CartObject, callback : @escaping (String) -> ()){
        
        var stringurl : String = ""
        if let urlhost = UserDefaults.standard.string(forKey: "http"){
            stringurl = "\(urlhost)/cart/insert"
        }
        
        let url = URL.init(string: stringurl)
        
        var header = [String : String]()
        header.updateValue("application/json", forKey: "Content-Type")
        
        let paramtable : [String : Any] = [
            "_id"           : cart.carttable.tableid,
            "name"          : cart.carttable.tablename
        ]
        
        var paramorder = [[String : Any]]()
        var notes : [NoteObject] = []
        for order in cart.cartorder{
            
//            for name in order.foodnote{
//                let note = NoteObject.init()
//                note.noteid = "5b90fc4091c64c13f890eca1"
//                note.notename = name
//
//                notes.append(note)
//            }
            
            let paramfood : [String : Any] = [
                "_id"       : order.foodid,
                "name"      : order.foodname,
                "price"     : order.foodprice,
                "saleOff"   : order.fooddiscount,
                "unitPrice" : order.foodcurrency
            ]
            let dictorder : [String : Any] = [
                "quantity"  : order.foodquantity,
                "subTotal"  : order.foodsubtotal,
                "discount"  : order.fooddiscount,
                "priority"  : order.foodpriority,
                "food"      : paramfood,
                "note"      : order.foodnote
            ]
            paramorder.append(dictorder)
        }
        
        let parameter : [String:Any] = [
            "id"            : cart.cartid,
            "subTotal"      : cart.cartsubtotal,
            "discount"      : cart.cartdiscount,
            "VAT"           : cart.carttax,
            "grandTotal"    : cart.cartgrandtotal,
            "status"        : cart.cartstatus,
            "table"         : paramtable,
            "order"         : paramorder
        ]
        
        
        Alamofire.request(url!, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON { (responseData) in
            if responseData.result.value != nil{
                let swiftyJsonVar = JSON(responseData.result.value!)

                let status =  swiftyJsonVar["status"]
                if status == "Success"{
                    callback("Success")
                }
                else{
                    callback("Failure")
                }
            }
        }
    }
    
    static func UpdateBookingOrder(cart : CartObject, callback : @escaping (String) -> ()){
        
        var stringurl : String = ""
        if let urlhost = UserDefaults.standard.string(forKey: "http"){
            stringurl = "\(urlhost)/cart/order/insert/\(cart.cartid)"
        }
        
        let url = URL.init(string: stringurl)
        
        var header = [String : String]()
        header.updateValue("application/json", forKey: "Content-Type")
        
        let paramtable : [String : Any] = [
            "_id"           : cart.carttable.tableid,
            "name"          : cart.carttable.tablename
        ]
        var notes : [NoteObject] = []
        var paramorder = [[String : Any]]()
        
        for order in cart.cartorder{
            
//            for name in order.foodnote{
//                let note = NoteObject.init()
//                note.noteid = "5b90fc4091c64c13f890eca1"
//                note.notename = name
//                
//                notes.append(note)
//            }
            
            let paramfood : [String : Any] = [
                "_id"       : order.foodid,
                "name"      : order.foodname,
                "price"     : order.foodprice,
                "saleOff"   : order.fooddiscount,
                "unitPrice" : order.foodcurrency
            ]
            let dictorder : [String : Any] = [
                "quantity"  : order.foodquantity,
                "subTotal"  : order.foodsubtotal,
                "discount"  : order.fooddiscount,
                "priority"  : order.foodpriority,
                "food"      : paramfood,
                "note"      : order.foodnote
            ]
            paramorder.append(dictorder)
        }
        
        let parameter : [String:Any] = [
            "id"            : cart.cartid,
            "subTotal"      : cart.cartsubtotal,
            "discount"      : cart.cartdiscount,
            "VAT"           : cart.carttax,
            "grandTotal"    : cart.cartgrandtotal,
            "status"        : cart.cartstatus,
            "table"         : paramtable,
            "order"         : paramorder
        ]
        
        Alamofire.request(url!, method: .put, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON { (responseData) in
            if responseData.result.value != nil{
                let swiftyJsonVar = JSON(responseData.result.value!)

                let status =  swiftyJsonVar["status"]
                if status == "Success"{
                    callback("Success")
                }
                else{
                    callback("Failure")
                }
            }
        }
    }
    
    static func IsOperationRealtimeData(responseString : String ,callback : @escaping (String)->()){
        let responseData = JSON.init(parseJSON: responseString)
        
        let operationtype = responseData["operationType"].stringValue
        
        callback(operationtype)
    }
    
    static func InsertRealtimeFood(responseString : String ,inserted : @escaping (FoodObject)->()){
        let responseData = JSON.init(parseJSON: responseString)
        
        // lay toan bo data can insert la fullDocument
        let dict = responseData["fullDocument"].dictionaryObject
        let foodobj = FoodObject.init(json: dict!)
        
        inserted(foodobj!)
    }
    
    static func UpdateRealTimeFood(responseString : String ,updated : @escaping (FoodObject)->()){
        
        let responseData = JSON.init(parseJSON: responseString)
        
        // update co 2 loai - updatefields
        let dict = responseData["fullDocument"].dictionaryObject
        let foodobj = FoodObject.init(json: dict!)
        // lay ve cateid (cateid lay sau vi data o 2 nhanh)
        let foodid = responseData["documentKey"].stringValue
        foodobj?.foodid = foodid
        
        updated(foodobj!)
        
    }
    
    static func DeleteRealTimeFood(responseString : String ,deleted : @escaping (String)->()){
        
        let responseData = JSON.init(parseJSON: responseString)
        
        let foodid = responseData["documentKey"].stringValue
        
        deleted(foodid)
        
    }
    
}
