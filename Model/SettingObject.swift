//
//  SettingObject.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 9/12/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SettingObject: NSObject {
    
    override init() {
        super.init()
    }
    
    static func GetStaticParameter(callback : @escaping (String, String, String, String, Double, String, String) -> ()){
        
        let header = [String : String]()
        
        var stringurl : String = ""
        if let urlhost = UserDefaults.standard.string(forKey: "http"){
            stringurl = "\(urlhost)/configure"
        }
        
        let url = URL.init(string: stringurl)
        
        Alamofire.request(url!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{ (responseData) -> Void in
            
            if (responseData.result.value != nil){
                let swiftyJsonVar = JSON(responseData.result.value!)
                print(swiftyJsonVar)
                let currency = swiftyJsonVar["status"]["selected"].stringValue
                let listcurrency = swiftyJsonVar["status"]["option"].stringValue
                
                let language = swiftyJsonVar["language"]["selected"].stringValue
                let listlanguage = swiftyJsonVar["language"]["option"].stringValue
                
                let vattmp = swiftyJsonVar["VAT"].stringValue
                guard let VAT = Double(vattmp) else {return}
                
                let icon = swiftyJsonVar["restaurant"]["icon"].stringValue
                let name = swiftyJsonVar["restaurant"]["name"].stringValue
                
                callback(currency, listcurrency, language, listlanguage, VAT, icon, name)
            }
        }
        
    }
    
    static func LoginAccount(username : String, password: String, callback : @escaping (Bool) -> ()){
        
        var stringurl : String = ""
        if let urlhost = UserDefaults.standard.string(forKey: "http"){
            stringurl = "\(urlhost)/login"
        }
        
        let parameter : [String : Any] = [
            "name"           : username,
            "password"       : password
            ]
        
        let url = URL.init(string: stringurl)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameter)
        
        Alamofire.request(request).responseJSON { (responseData) in
            if responseData.result.value != nil{
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                let status =  swiftyJsonVar["auth"].boolValue
                callback(status)
            }
        }
        
//        Alamofire.request(url!, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON{ (responseData) -> Void in
//
//            if responseData.result.value != nil{
//                let swiftyJsonVar = JSON(responseData.result.value!)
//
//                let status =  swiftyJsonVar["auth"].boolValue
//                callback(status)
//            }
//
//        }
    }
    
//    static func ChangePassword( username : String, password : String, callback : @escaping (String)){
//        
//    }
}
