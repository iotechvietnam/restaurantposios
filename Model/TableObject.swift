//
//  TableObject.swift
//  RestaurantPOS
//
//  Created by ltk1205 on 8/14/18.
//  Copyright © 2018 IOTech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TableObject: NSObject {

    var tableid : String = ""
    var tablename : String = ""
    
    override init() {
        super.init()
    }
    
    init?(json : [String:Any]){
        if let tableid = json["_id"] as? String{
            self.tableid = tableid
        }
        if let tablename = json["name"] as? String{
            self.tablename = tablename
        }
    }
        
    static func GetListTable(callback : @escaping ([TableObject]) -> ()){
        
        let header = [String : String]()
        
        var stringurl : String = ""
        if let urlhost = UserDefaults.standard.string(forKey: "http"){
            stringurl = "\(urlhost)/tables"
        }
        
        let url = URL.init(string: stringurl)
        
        Alamofire.request(url!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON{ (responseData) -> Void in
            
            var tables = [TableObject]()
            if (responseData.result.value != nil){
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                for case let result in swiftyJsonVar.arrayValue{
                    let dict = result.dictionaryObject
                    let table = TableObject.init(json: dict!)
                    tables.append(table!)
                }
                callback(tables)
            }
        }
    }
}
